use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::incoming_message::IncomingMessage;
use crate::plugins::admin_plugin::{AdminPlugin, AdminResponse};
use async_trait::async_trait;

pub struct SystemAdminPlugin<Db: Database> {
    _db: DatabaseWrapper<Db>,
}

impl<Db: Database> SystemAdminPlugin<Db> {
    pub async fn new(db: DatabaseWrapper<Db>) -> Self {
        Self { _db: db }
    }
}

#[async_trait]
impl<Db: Database + Send> AdminPlugin for SystemAdminPlugin<Db> {
    async fn message(
        &mut self,
        incoming: &IncomingMessage,
    ) -> anyhow::Result<AdminResponse> {
        if incoming.text == "exit" {
            Ok(AdminResponse::exit("Exiting", 0))
        } else if incoming.text.starts_with("!print_error ") {
            // TODO: parse properly
            Ok(AdminResponse::error(
                "",
                "system_admin_plugin",
                &incoming.text[13..],
            ))
        } else if incoming.text == "!access_token" {
            Ok(AdminResponse::matrix_access_token())
        } else {
            Ok(AdminResponse::none())
        }
    }
}
