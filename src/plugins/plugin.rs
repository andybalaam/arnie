use async_trait::async_trait;

use crate::incoming_message::IncomingMessage;
use crate::outgoing_message::OutgoingMessage;

#[async_trait]
pub trait Plugin {
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>>;
}
