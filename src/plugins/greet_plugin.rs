use anyhow::anyhow;
use rand::rngs::SmallRng;
use rand::seq::SliceRandom;
use rand::{Rng, SeedableRng};
use serde::{Deserialize, Serialize};
use std::sync::{Arc, Mutex};
use std::time::Duration;
use tokio::sync::mpsc;

use async_trait::async_trait;

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::human_command::HumanCommand;
use crate::incoming_message::IncomingMessage;
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::plugin::Plugin;
use crate::room_address::RoomAddress;
use crate::scheduler::Scheduler;

const GREETINGS_KEY: &str = "greetings";

pub struct GreetPlugin<Db, R>
where
    Db: Database,
    R: Rng + Clone + Send + Sync + 'static,
{
    db: DatabaseWrapper<Db>,
    messages_channel_sender: mpsc::UnboundedSender<OutgoingMessage>,
    rng: R,
    scheduler: Arc<dyn Scheduler>,
}

impl<Db> GreetPlugin<Db, SmallRng>
where
    Db: Database,
{
    pub async fn new(
        db: DatabaseWrapper<Db>,
        messages_channel_sender: mpsc::UnboundedSender<OutgoingMessage>,
        scheduler: Arc<dyn Scheduler>,
    ) -> GreetPlugin<Db, SmallRng> {
        GreetPlugin::<Db, SmallRng>::with_rng(
            db,
            messages_channel_sender,
            scheduler,
            SmallRng::from_entropy(),
        )
        .await
    }
}

impl<Db, R> GreetPlugin<Db, R>
where
    Db: Database,
    R: Rng + Clone + Send + Sync + 'static,
{
    pub async fn with_rng(
        db: DatabaseWrapper<Db>,
        messages_channel_sender: mpsc::UnboundedSender<OutgoingMessage>,
        scheduler: Arc<dyn Scheduler>,
        rng: R,
    ) -> GreetPlugin<Db, R> {
        // TODO: unwrap
        let greetings = db
            .lrange(GREETINGS_KEY, 0, -1)
            .await
            .expect("Database error reading greetings");

        for greeting in greetings {
            let greeting_config: Result<GreetingConfig, _> =
                serde_json::from_str(&greeting);

            match greeting_config {
                Ok(greeting_config) => {
                    let sched_res = schedule_message(
                        messages_channel_sender.clone(),
                        scheduler.as_ref(),
                        rng.clone(),
                        greeting_config,
                    )
                    .await;

                    if let Err(_) = sched_res {
                        // This should not happen - some invalid config got
                        // saved to the DB.
                        println!("Failed to schedule greeting!");
                    }
                }
                Err(e) => {
                    println!("Failed to parse GreetingConfig in greet: {}", e);
                }
            }
        }

        GreetPlugin {
            db,
            messages_channel_sender,
            scheduler,
            rng,
        }
    }

    async fn run_remove(
        &mut self,
        content: &str,
        room_address: &RoomAddress,
    ) -> anyhow::Result<String> {
        let Ok(num_within_room) = content.parse() else {
            return Ok("Supply an index of the greeting to remove".to_owned());
        };

        let db_index =
            self.find_db_index(num_within_room, room_address).await?;

        let res = self.remove_greeting(db_index).await;
        match res {
            Ok(_) => {
                unschedule_message(self.scheduler.as_ref(), db_index).await;
                Ok("Greeting terminated.".to_owned())
            }
            Err(e) => Ok(format!("Error: {e}")),
        }
    }

    async fn find_db_index(
        &mut self,
        num_within_room: isize,
        room_address: &RoomAddress,
    ) -> anyhow::Result<isize> {
        let greeting_configs = self.db.lrange(GREETINGS_KEY, 0, -1).await?;
        // Iterate through configs, mapping ones in this room to their
        // db index.
        let idx = greeting_configs
            .into_iter()
            .enumerate()
            .filter_map(|(db_index, cfg)| {
                let cfg: serde_json::Result<GreetingConfig> =
                    serde_json::from_str(&cfg);
                if let Ok(cfg) = cfg {
                    let ra: serde_json::Result<RoomAddress> =
                        serde_json::from_str(&cfg.room_address);
                    if let Ok(ra) = ra {
                        if ra == *room_address {
                            return Some(db_index as isize);
                        }
                    }
                }
                None
            })
            // Take the nth one in the room and return its db index
            .nth(num_within_room as usize);
        idx.ok_or(anyhow!("Invalid index!"))
    }
}

/// A recurring time period in which a message will be
/// sent, consisting of a start time, and a duration.
/// The message will be sent at a random time between
/// the start and the start + duration.
/// The start time is actually a cron-style schedule
/// because this represents a recurring time.
#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
struct RecurringTimeRange {
    /// Cron-style schedule e.g.
    /// "0 30 9 * * Mon,Tue,Wed,Thu,Fri *"
    pub start_time: String,

    /// How long in minutes after the start we will
    /// definitely have sent the message. (It is sent
    /// at a random time between the start_time and
    /// this long afterwards.)
    pub duration_mins: u64,
}

/// Add a message to our scheduler.
/// time is a cron-formatted time when messages should be sent
/// time_range_mins is
async fn schedule_message<R: Rng + Clone + Send + Sync + 'static>(
    messages_channel_sender: mpsc::UnboundedSender<OutgoingMessage>,
    scheduler: &dyn Scheduler,
    mut rng: R,
    greeting_config: GreetingConfig,
) -> Result<(), ()> {
    // Scheduler requires our sender to be Sync, because
    // tokio_cron_scheduler has the same requirement. We think this
    // shouldn't be needed but we can work around it here with a
    // Mutex. In future we might want to consider sync_wrapper::SyncWrapper
    // as a more convenient.
    let sender_clone = Arc::new(Mutex::new(messages_channel_sender));

    let start_time = &greeting_config.time_range.start_time;
    let duration_mins = greeting_config.time_range.duration_mins;

    scheduler
        .add(
            start_time,
            Box::new(move || {
                let room_address = greeting_config.room_address.clone();
                let room_address = serde_json::from_str(&room_address)
                    .expect("Unable to deserialize room address");
                let greetings = greeting_config.greetings.clone();
                let sender_clone = sender_clone.clone();
                let secs_to_sleep = if duration_mins == 0 {
                    0
                } else {
                    rng.gen_range(0..duration_mins) * 60
                };
                let greeting = greetings
                    .choose(&mut rng)
                    .cloned()
                    .unwrap_or_else(|| String::from("Awight"));

                Box::pin(async move {
                    tokio::time::sleep(Duration::from_secs(secs_to_sleep))
                        .await;

                    let msg = OutgoingMessage::new(greeting, room_address);

                    sender_clone
                        .lock()
                        .unwrap()
                        .send(msg)
                        .expect("Failed to queue message to send");
                })
            }),
        )
        .await
}

async fn unschedule_message(scheduler: &dyn Scheduler, index: isize) {
    scheduler
        .remove(
            index.try_into().expect(
                "index is negative but we should have checked it by now!",
            ),
        )
        .await
}

impl<Db, R> GreetPlugin<Db, R>
where
    Db: Database,
    R: Rng + Clone + Send + Sync + 'static,
{
    async fn process_command(
        &mut self,
        command: HumanCommand,
        room_address: &RoomAddress,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        let subcommand = command.keywords.get(1);

        let quote = match subcommand.map(String::as_str) {
            Some("!help") => self.help().await,
            Some("!list") => self.list_greetings(room_address).await?,
            Some("!remove") => {
                self.run_remove(&command.content(), room_address).await?
            }
            Some(_) => "Error: unknown command, screw you!".to_owned(),
            None => self.add_greeting(room_address, command).await?,
        };

        Ok(Some(OutgoingMessage::new(quote, room_address.clone())))
    }

    async fn help(&mut self) -> String {
        "\
            !greet [time] [time_range] [greetings...]\n\
            Add a regular greeting\n\
            \n\
            time = a crontab-format scheduled time (use quotes)\n\
                   (default = 9:30am every week day)\n\
            time_range = max. minutes after scheduled time to greet (randomly chosen)\n\
                         (default = 60 minutes)\n\
            greetings = quoted strings for alternatives Arnie should say\n\
                        (default = \"Awight\")\n\
            \n\
            Examples:\n\
            \n\
            !greet\n\
            !greet \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"Awight\" \"Good morning!\"\n\
            \n\
            !greet !list\n\
            List all the greetings in this room.\n\
            \n\
            !greet !remove <number>\n\
            Remove a greeting - use !greet !list to find the number of\n\
            the greeting you want to remove.\n\
            \n\
            !greet !help\n\
            Display this help message.\n\
        ".to_owned()
    }

    async fn list_greetings(
        &mut self,
        room_address: &RoomAddress,
    ) -> anyhow::Result<String> {
        let greeting_configs = self.db.lrange(GREETINGS_KEY, 0, -1).await?;
        Ok(greeting_configs
            .into_iter()
            .filter_map(|cfg| {
                let cfg: serde_json::Result<GreetingConfig> =
                    serde_json::from_str(&cfg);

                if let Ok(cfg) = cfg {
                    let ra: serde_json::Result<RoomAddress> =
                        serde_json::from_str(&cfg.room_address);
                    if let Ok(ra) = ra {
                        if ra == *room_address {
                            return Some(cfg.description());
                        }
                    }
                }
                None
            })
            .enumerate()
            .map(|(i, desc)| format!("[{i}] {desc}"))
            .collect::<Vec<_>>()
            .join("\n"))
    }

    async fn add_greeting(
        &mut self,
        room_address: &RoomAddress,
        command: HumanCommand,
    ) -> anyhow::Result<String> {
        let start_time: &str = command
            .args
            .get(0)
            // Turn it into a &str instead of &String
            .map(|s| &**s)
            // If it's "" make it None
            .and_then(|s| if s == "" { None } else { Some(s) })
            // If it's None, default to a sensible schedule
            .unwrap_or("0 30 9 * * Mon,Tue,Wed,Thu,Fri *");

        let duration_mins: Option<&String> = command.args.get(1);
        let duration_mins: u64 =
            duration_mins.map(|s| s.parse().unwrap_or(60)).unwrap_or(60);

        let time_range = RecurringTimeRange {
            start_time: start_time.to_owned(),
            duration_mins,
        };

        let greetings = if command.args.len() > 1 {
            &command.args[2..]
        } else {
            &[]
        };

        let greeting_config = GreetingConfig {
            room_address: serde_json::to_string(&room_address)
                .expect("Failed to serialize room address"),
            greetings: greetings.into(),
            time_range,
        };

        let sched_res = schedule_message(
            self.messages_channel_sender.clone(),
            self.scheduler.as_ref(),
            self.rng.clone(),
            greeting_config.clone(),
        )
        .await;

        if let Err(_) = sched_res {
            Ok("Error: Schedule was invalid: must be crontab-like \
                e.g. \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\""
                .to_owned())
        } else {
            self.db
                .lpush(
                    GREETINGS_KEY,
                    &serde_json::to_string(&greeting_config)
                        .expect("Failed to serialize GreetingConfig"),
                )
                .await?;
            Ok("Greeting added.".to_owned())
        }
    }

    async fn remove_greeting(
        &mut self,
        index: isize,
    ) -> Result<GreetingConfig, String> {
        let greetings_len =
            self.db.llen(GREETINGS_KEY).await.map_err(|_| {
                String::from("Error: failed to find number of greetings")
            })?;

        if index < 0 || index >= greetings_len as isize {
            return Err(format!(
                "Try using a number from 0-{} inclusive.",
                greetings_len - 1
            ));
        }

        let greeting = self
            .db
            .lindex(GREETINGS_KEY, index)
            .await
            .map_err(|_| String::from("Error: couldn't find greeting."))?;

        if let Some(greeting) = greeting {
            self.db
                .lrem(GREETINGS_KEY, 1, &greeting)
                .await
                .map_err(|_| "Couldn't remove quote".to_owned())
                .and_then(|_| {
                    serde_json::from_str(&greeting).map_err(|_| {
                        "Failed to parse greeting from DB.".to_owned()
                    })
                })
        } else {
            Err(format!("Could not find greeting with index {}", index))
        }
    }
}

#[async_trait]
impl<Db, R> Plugin for GreetPlugin<Db, R>
where
    Db: Database + Send,
    R: Rng + Clone + Send + Sync + 'static,
{
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        if let Some(parsed) = HumanCommand::parse(&message.text) {
            if let Some(keyword) = parsed.keywords.get(0) {
                if keyword == "!greet" {
                    return self
                        .process_command(parsed, &message.room_address)
                        .await;
                }
            }
        }
        Ok(None)
    }
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
struct GreetingConfig {
    room_address: String,
    greetings: Vec<String>,
    time_range: RecurringTimeRange,
}

impl GreetingConfig {
    fn description(&self) -> String {
        format!(
            "\"{}\" {} \"{}\"",
            self.time_range.start_time,
            self.time_range.duration_mins,
            self.greetings.get(0).map(|s| &**s).unwrap_or("")
        )
    }
}

#[cfg(test)]
mod test {
    use std::{
        future::Future,
        pin::Pin,
        sync::{Arc, Mutex},
    };

    use assert_matches2::assert_matches;
    use rand::RngCore;
    use tokio::sync::mpsc::{error::TryRecvError, UnboundedReceiver};

    use crate::memory_database::MemoryDatabase;

    use super::*;

    #[derive(Clone)]
    struct FakeRng {
        hard_coded_answer: u32,
    }

    impl FakeRng {
        /**
         * Create a fake random number generator that returns the numbers
         * you passed in for hard_coded_answers.
         */
        fn new(hard_coded_answer: u32) -> Self {
            FakeRng { hard_coded_answer }
        }
    }

    impl RngCore for FakeRng {
        fn next_u32(&mut self) -> u32 {
            self.hard_coded_answer
        }

        fn next_u64(&mut self) -> u64 {
            self.hard_coded_answer.into()
        }

        fn fill_bytes(&mut self, _dest: &mut [u8]) {
            todo!();
        }

        fn try_fill_bytes(
            &mut self,
            _dest: &mut [u8],
        ) -> Result<(), rand::Error> {
            todo!();
        }
    }

    #[derive(Clone)]
    struct FakeScheduler {
        jobs: Arc<Mutex<Vec<String>>>,
        failing: bool,
    }

    impl FakeScheduler {
        fn new() -> Self {
            Self {
                jobs: Arc::new(Mutex::new(Vec::new())),
                failing: false,
            }
        }

        fn new_failing() -> Self {
            Self {
                jobs: Arc::new(Mutex::new(Vec::new())),
                failing: true,
            }
        }

        fn jobs(&self) -> Vec<String> {
            self.jobs.lock().unwrap().clone()
        }
    }

    #[async_trait]
    impl Scheduler for FakeScheduler {
        async fn add(
            &self,
            schedule: &str,
            mut f: Box<
                dyn FnMut() -> Pin<Box<dyn Future<Output = ()> + Send>>
                    + Send
                    + Sync,
            >,
        ) -> Result<(), ()> {
            if self.failing {
                return Err(());
            }

            self.jobs.lock().unwrap().push(String::from(schedule));

            // Call f immediately, so we can check its results
            f().await;

            Ok(())
        }

        async fn remove(&self, index: usize) {
            self.jobs.lock().unwrap().remove(index);
        }

        fn start(&self) -> anyhow::Result<()> {
            Ok(())
        }
    }

    #[tokio::test]
    async fn on_startup_we_schedule_a_task() {
        let scheduler = Arc::new(FakeScheduler::new());
        create_greet(&[create_greeting_config()], Arc::clone(&scheduler)).await;

        let jobs = scheduler.jobs();
        assert_eq!(jobs[0], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs.len(), 1);
    }

    #[tokio::test]
    async fn when_triggered_the_task_sends_a_message() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup =
            create_greet(&[create_greeting_config()], Arc::clone(&scheduler))
                .await;

        // (The FakeScheduler called our callback immediately.)

        let message = setup
            .receiver
            .recv()
            .await
            .expect("Failed to receive message");
        assert_eq!(message.text, "Awight");
        assert_eq!(message.room_address, RoomAddress::CommandLine);

        let res = setup.receiver.try_recv();
        assert_matches!(res, Err(TryRecvError::Empty));
    }

    #[tokio::test]
    async fn can_add_default_greeting() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // Tell arnie to greet us in this room
        let resp = command_response(&mut setup.plugin, "!greet").await;

        // He should respond positively
        assert_eq!(resp, "Greeting added.");

        // A job should have been scheduled
        let jobs = scheduler.jobs();
        assert_eq!(jobs[0], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs.len(), 1);
    }

    #[tokio::test]
    async fn can_add_default_greeting_with_no_duration() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // Tell arnie to greet us in this room
        let resp =
            command_response(&mut setup.plugin, r#"!greet "0 10 9 * * * *""#)
                .await;

        // He should respond positively
        assert_eq!(resp, "Greeting added.");

        // A job should have been scheduled
        let jobs = scheduler.jobs();
        assert_eq!(jobs[0], "0 10 9 * * * *");
        assert_eq!(jobs.len(), 1);
    }

    #[tokio::test]
    async fn can_add_default_greeting_with_duration() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // When we tell arnie to greet us with a specific time range
        let resp = command_response(
            &mut setup.plugin,
            r#"!greet "0 11 9 * * * *" 10"#,
        )
        .await;
        assert_eq!(resp, "Greeting added.");

        // Then he stores the time range in the DB
        let stored_config = setup.get_config_from_db(0).await;

        assert_eq!(stored_config.time_range.duration_mins, 10);
        assert_eq!(stored_config.time_range.start_time, "0 11 9 * * * *");
    }

    #[tokio::test]
    async fn empty_string_arguments_for_times_give_default() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // When we tell arnie to greet us with empty strings for times
        let resp = command_response(
            &mut setup.plugin,
            r#"!greet "" "" "greeting 1" "greeting 2""#,
        )
        .await;
        assert_eq!(resp, "Greeting added.");

        // Then he stores the default time range in the DB
        let stored_config = setup.get_config_from_db(0).await;

        assert_eq!(stored_config.time_range.duration_mins, 60);
        assert_eq!(
            stored_config.time_range.start_time,
            "0 30 9 * * Mon,Tue,Wed,Thu,Fri *"
        );
    }

    #[tokio::test]
    async fn invalid_schedules_dont_crash_and_display_error_to_user() {
        // Given our scheduler is going to fail when we try to add a
        // scheduled task.
        let scheduler = Arc::new(FakeScheduler::new_failing());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // When we tell arnie to greet us
        let resp = command_response(
            &mut setup.plugin,
            r#"!greet "any_time_because_fake_scheduler_fails" 55 "g1" "g2""#,
        )
        .await;

        // Then we receive and error
        assert_eq!(
            resp,
            "Error: Schedule was invalid: must be crontab-like \
            e.g. \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\""
        );

        // And this greeting was not saved in the DB
        assert_eq!(setup.num_saved_greetings().await, 0);
    }

    #[tokio::test]
    async fn can_show_help_text() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // Tell arnie to greet us in this room
        let resp = command_response(&mut setup.plugin, "!greet !help").await;

        // He should respond positively
        assert_eq!(
            resp,
            "\
                !greet [time] [time_range] [greetings...]\n\
                Add a regular greeting\n\
                \n\
                time = a crontab-format scheduled time (use quotes)\n\
                       (default = 9:30am every week day)\n\
                time_range = max. minutes after scheduled time to greet (randomly chosen)\n\
                             (default = 60 minutes)\n\
                greetings = quoted strings for alternatives Arnie should say\n\
                            (default = \"Awight\")\n\
                \n\
                Examples:\n\
                \n\
                !greet\n\
                !greet \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"Awight\" \"Good morning!\"\n\
                \n\
                !greet !list\n\
                List all the greetings in this room.\n\
                \n\
                !greet !remove <number>\n\
                Remove a greeting - use !greet !list to find the number of\n\
                the greeting you want to remove.\n\
                \n\
                !greet !help\n\
                Display this help message.\n\
            "
        );

        // Nothing should have been scheduled
        let jobs = scheduler.jobs();
        assert_eq!(jobs.len(), 0);
    }

    #[tokio::test]
    async fn can_add_greetings_at_custom_times() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;

        // Tell arnie to greet us in this room
        let resp = command_response(
            &mut setup.plugin,
            "!greet \"1 15 10 * * * *\" 60",
        )
        .await;

        // He should respond positively
        assert_eq!(resp, "Greeting added.");

        // A job should have been scheduled
        let jobs = scheduler.jobs();
        assert_eq!(jobs[0], "1 15 10 * * * *");
        assert_eq!(jobs.len(), 1);
    }

    #[tokio::test]
    async fn can_list_greetings() {
        // Given a plugin whose DB contains 2 greetings
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(
            &[create_greeting_config(), create_greeting_config()],
            Arc::clone(&scheduler),
        )
        .await;

        // Tell arnie to list the greetings he has set up
        let resp = command_response(&mut setup.plugin, "!greet !list").await;

        // He should list them
        assert_eq!(
            resp,
            "\
            [0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"\"\n\
            [1] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"\"\
            "
        );
    }

    #[tokio::test]
    async fn can_only_list_greetings_in_this_room() {
        let this_room = RoomAddress::CommandLine;
        let other_room = RoomAddress::Matrix("!r:s.co".to_owned());

        // Given a plugin whose DB contains greetings from this
        // room and another
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(
            &[
                create_greeting_config_details(&this_room, "hi here"),
                create_greeting_config_details(&other_room, "hi there"),
                create_greeting_config_details(&this_room, "yo here"),
                create_greeting_config_details(&other_room, "yo there"),
            ],
            Arc::clone(&scheduler),
        )
        .await;

        // When we tell arnie to list the greetings
        let resp = command_response(&mut setup.plugin, "!greet !list").await;

        // Then he lists only the ones in this room
        assert_eq!(
            resp,
            "\
            [0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"yo here\"\n\
            [1] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"hi here\"\
            "
        );
    }

    #[tokio::test]
    async fn can_add_multiple_greetings_in_different_rooms() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;
        let r1 = RoomAddress::Matrix("!r:s.co".to_owned());
        let r2 = RoomAddress::CommandLine;
        let r3 = RoomAddress::Matrix("!a:a.aa".to_owned());

        // Tell arnie to greet us several rooms
        command_response_in_room(
            &mut setup.plugin,
            "!greet \"\" \"\" \"a\"",
            r1.clone(),
        )
        .await;
        command_response_in_room(
            &mut setup.plugin,
            "!greet \"\" \"\" \"b\"",
            r2.clone(),
        )
        .await;
        command_response_in_room(
            &mut setup.plugin,
            "!greet \"\" \"\" \"c\"",
            r3.clone(),
        )
        .await;

        // When we list them then we can see he has stored them
        assert_eq!(
            command_response_in_room(&mut setup.plugin, "!greet !list", r1)
                .await,
            "[0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"a\""
        );
        assert_eq!(
            command_response_in_room(&mut setup.plugin, "!greet !list", r2)
                .await,
            "[0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"b\""
        );
        assert_eq!(
            command_response_in_room(&mut setup.plugin, "!greet !list", r3)
                .await,
            "[0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"c\""
        )
    }

    #[tokio::test]
    async fn can_customise_the_greeting() {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(&[], Arc::clone(&scheduler)).await;
        // Tell arnie to greet us several rooms
        command_response_in_room(
            &mut setup.plugin,
            r#"!greet "" "" "Get OUT!""#,
            RoomAddress::Matrix("!r:s.co".to_owned()),
        )
        .await;

        let message = setup
            .receiver
            .recv()
            .await
            .expect("Failed to receive message");
        assert_eq!(message.text, "Get OUT!");
    }

    #[tokio::test]
    async fn can_add_multiple_strings_to_a_greeting() {
        let rng1 = FakeRng::new(0);
        let greeting1 =
            extract_greeting(r#"!greet "" "" "Greeting 1" "Greeting 2""#, rng1)
                .await;

        assert_eq!(greeting1, "Greeting 1");

        // This hard-coded value persuades the choose() function to pick
        // our second greeting. It would be nice to avoid knowing so much
        // about how this works, but when we naively supplied different
        // values choose() ended up calling next_u32() over and over again,
        // so we had to learn quick about how it worked under the hood,
        // and this number is the result.
        let rng2 = FakeRng::new(2147483649);
        let greeting2 =
            extract_greeting(r#"!greet "" "" "Greeting 1" "Greeting 2""#, rng2)
                .await;

        assert_eq!(greeting2, "Greeting 2");
    }

    async fn extract_greeting(creation_string: &str, rng: FakeRng) -> String {
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup =
            create_greet_rng(&[], Arc::clone(&scheduler), rng).await;

        // Tell arnie to greet us several rooms
        command_response_in_room(
            &mut setup.plugin,
            creation_string,
            RoomAddress::Matrix("!r:s.co".to_owned()),
        )
        .await;

        setup
            .receiver
            .recv()
            .await
            .expect("Failed to receive message")
            .text
    }

    #[tokio::test]
    async fn can_remove_greetings_by_number() {
        // Given some other greeting in another room already exists
        let room_address = RoomAddress::Matrix("!rZZZ:s.com".to_owned());
        let scheduler = Arc::new(FakeScheduler::new());
        let mut setup = create_greet(
            &[create_greeting_config_details(
                &room_address,
                "other room, hi!",
            )],
            Arc::clone(&scheduler),
        )
        .await;

        // Tell arnie to greet us several rooms
        command_response(&mut setup.plugin, "!greet \"\" \"\" \"a\"").await;
        command_response(&mut setup.plugin, "!greet \"\" \"\" \"b\"").await;
        command_response(&mut setup.plugin, "!greet \"\" \"\" \"c\"").await;
        command_response_in_room(
            &mut setup.plugin,
            "!greet \"\" \"\" \"other room d\"",
            room_address,
        )
        .await;

        // Sanity: they were all scheduled
        let jobs = scheduler.jobs();
        assert_eq!(jobs[0], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[1], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[2], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[3], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[4], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs.len(), 5);

        // Sanity: they exist
        let resp = command_response(&mut setup.plugin, "!greet !list").await;
        assert_eq!(
            resp,
            "\
            [0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"c\"\n\
            [1] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"b\"\n\
            [2] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"a\"\
            "
        );

        // When we remove one
        let resp =
            command_response(&mut setup.plugin, "!greet !remove 1").await;

        assert_eq!(resp, "Greeting terminated.");

        // Then it is gone
        let resp = command_response(&mut setup.plugin, "!greet !list").await;
        assert_eq!(
            resp,
            "\
            [0] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"c\"\n\
            [1] \"0 30 9 * * Mon,Tue,Wed,Thu,Fri *\" 60 \"a\"\
            "
        );

        // And they were unscheduled
        let jobs = scheduler.jobs();
        assert_eq!(jobs[0], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[1], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[2], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs[3], "0 30 9 * * Mon,Tue,Wed,Thu,Fri *");
        assert_eq!(jobs.len(), 4);
    }

    async fn command_response(
        plugin: &mut GreetPlugin<MemoryDatabase, FakeRng>,
        command: &str,
    ) -> String {
        let msg = IncomingMessage::new(
            command.to_owned(),
            RoomAddress::CommandLine,
            "myname",
        );
        plugin
            .message(&msg)
            .await
            .expect("Error from message method")
            .expect("No reply from message method")
            .text
            .to_owned()
    }

    async fn command_response_in_room(
        plugin: &mut GreetPlugin<MemoryDatabase, FakeRng>,
        command: &str,
        room_address: RoomAddress,
    ) -> String {
        let msg =
            IncomingMessage::new(command.to_owned(), room_address, "myname");
        plugin
            .message(&msg)
            .await
            .expect("Error from message method")
            .expect("No reply from message method")
            .text
            .to_owned()
    }

    struct GreetTestSetup {
        plugin: GreetPlugin<MemoryDatabase, FakeRng>,
        receiver: UnboundedReceiver<OutgoingMessage>,
    }

    impl GreetTestSetup {
        async fn get_config_from_db(&self, index: isize) -> GreetingConfig {
            serde_json::from_str(
                &self
                    .plugin
                    .db
                    .lindex(GREETINGS_KEY, index)
                    .await
                    .unwrap()
                    .unwrap(),
            )
            .unwrap()
        }

        async fn num_saved_greetings(&self) -> usize {
            self.plugin.db.llen(GREETINGS_KEY).await.unwrap()
        }
    }

    /// Set up a DB to ask for a single greeting, and create a greet
    async fn create_greet<'a>(
        greetings_to_insert: &[GreetingConfig],
        scheduler: Arc<FakeScheduler>,
    ) -> GreetTestSetup {
        create_greet_rng(greetings_to_insert, scheduler, FakeRng::new(0)).await
    }

    async fn create_greet_rng(
        greetings_to_insert: &[GreetingConfig],
        scheduler: Arc<FakeScheduler>,
        rng: FakeRng,
    ) -> GreetTestSetup {
        let (messages_channel_sender, messages_channel_receiver) =
            mpsc::unbounded_channel();
        let mut db = DatabaseWrapper::new(
            Arc::new(tokio::sync::Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        );
        for greeting in greetings_to_insert {
            db.lpush(GREETINGS_KEY, &serde_json::to_string(greeting).unwrap())
                .await
                .unwrap();
        }

        GreetTestSetup {
            plugin: GreetPlugin::with_rng(
                db,
                messages_channel_sender.clone(),
                scheduler,
                rng,
            )
            .await,
            receiver: messages_channel_receiver,
        }
    }

    fn create_greeting_config() -> GreetingConfig {
        create_greeting_config_details(&RoomAddress::CommandLine, "")
    }

    fn create_greeting_config_details(
        room_address: &RoomAddress,
        greeting: &str,
    ) -> GreetingConfig {
        let greetings = if greeting.is_empty() {
            vec![]
        } else {
            vec![greeting.to_owned()]
        };

        GreetingConfig {
            room_address: serde_json::to_string(room_address).unwrap(),
            greetings,
            time_range: RecurringTimeRange {
                start_time: "0 30 9 * * Mon,Tue,Wed,Thu,Fri *".to_owned(),
                duration_mins: 60,
            },
        }
    }
}
