use async_trait::async_trait;
use chrono::{DateTime, Utc};

use crate::human_command::HumanCommand;
use crate::incoming_message::IncomingMessage;
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::plugin::Plugin;
use crate::room_address::RoomAddress;

pub struct DatePlugin<Nf: NowFinder> {
    now_finder: Nf,
}

impl DatePlugin<RealNowFinder> {
    pub fn new_real() -> Self {
        Self {
            now_finder: RealNowFinder {},
        }
    }
}

pub trait NowFinder {
    fn now(&self) -> DateTime<Utc>;
}

pub struct RealNowFinder {}

impl NowFinder for RealNowFinder {
    fn now(&self) -> DateTime<Utc> {
        Utc::now()
    }
}

enum Cmd {
    Help,
    Utc,
    Local,
}

fn cmd(s: &Option<&String>) -> Cmd {
    if let Some(s) = s {
        match s.as_str() {
            "!help" => Cmd::Help,
            "!utc" => Cmd::Utc,
            "!local" => Cmd::Local,
            _ => Cmd::Help,
        }
    } else {
        Cmd::Utc
    }
}

impl<Nf: NowFinder> DatePlugin<Nf> {
    async fn process_command(
        &mut self,
        command: HumanCommand,
        room_address: &RoomAddress,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        let c = cmd(&command.keywords.get(1));
        let ans = match c {
            Cmd::Help => "Usage: !date [!utc|!local|!help]".to_owned(),
            Cmd::Utc => self
                .now_finder
                .now()
                .format("UTC: %Y-%m-%d %H:%M:%S%:z")
                .to_string(),
            Cmd::Local => self
                .now_finder
                .now()
                .format("Arnie local: %Y-%m-%d %H:%M:%S%:z")
                .to_string(),
        };
        Ok(Some(OutgoingMessage::new(ans, room_address.clone())))
    }
}

#[async_trait]
impl<Nf: NowFinder + Send> Plugin for DatePlugin<Nf> {
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        if let Some(parsed) = HumanCommand::parse(&message.text) {
            if let Some(keyword) = parsed.keywords.get(0) {
                if keyword == "!date" {
                    return self
                        .process_command(parsed, &message.room_address)
                        .await;
                }
            }
        }
        Ok(None)
    }
}

#[cfg(test)]
mod test {
    use chrono::TimeZone;

    use super::*;

    impl DatePlugin<FakeNowFinder> {
        fn new_fake() -> Self {
            Self {
                now_finder: FakeNowFinder {
                    now: Utc.with_ymd_and_hms(2024, 11, 28, 18, 5, 30).unwrap(),
                },
            }
        }
    }

    #[tokio::test]
    async fn can_get_date() {
        let mut date_plugin = DatePlugin::new_fake();
        let result = date_plugin
            .message(&IncomingMessage::new(
                "!date !utc".to_owned(),
                RoomAddress::CommandLine,
                "fake_from".to_owned(),
            ))
            .await
            .expect("Error sending message")
            .expect("Unexpected empty response");

        assert_eq!(
            result,
            OutgoingMessage::new(
                "UTC: 2024-11-28 18:05:30+00:00".to_owned(),
                RoomAddress::CommandLine
            )
        );
    }

    struct FakeNowFinder {
        now: DateTime<Utc>,
    }

    impl NowFinder for FakeNowFinder {
        fn now(&self) -> DateTime<Utc> {
            self.now.clone()
        }
    }
}
