use std::fmt::Display;

use async_trait::async_trait;
use itertools::Itertools;
use rand::rngs::SmallRng;
use rand::Rng;
use rand::SeedableRng;

use crate::args::VERSION;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::human_command::HumanCommand;
use crate::incoming_message::IncomingMessage;
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::plugin::Plugin;
use crate::room_address::RoomAddress;

const SCHEMA_VERSION: usize = 2;
const INSTALLED_KEY: &str = "installed";
const QUOTES_KEY_PREFIX: &str = "person_quotes|";
const PEOPLE_KEY: &str = "people";
const ARNIE_NAME: &str = "arnie";

const DEFAULT_QUOTES: [&str; 29] = [
    "I need a vacation.",
    "Let off some steam, Bennett.",
    "You’ve just been erased.",
    "Get your ass to Mars!",
    "It's not a toomah!",
    "No problemo.",
    "Screw you!",
    "Chill out, d___wad.",
    "Come with me if you want to live.",
    "If it bleeds, we can kill it.",
    "Do it. Do it now!",
    "See you at the party, Richter!",
    "Get to da choppa!",
    "Hasta la vista, baby.",
    "Consider that a divorce.",
    "I'll be back.",
    "To crush your enemies, see dem driven before you, and to hear de \
    lamentation of der women.",
    "Remember when I said I'd kill you last?  I lied.",
    "You should not drink, and bake.",
    "CROM",
    "You got what you want Cohagen, give dese people air!",
    "Don't disturb my friend, he's dead tired.",
    "You blew my cover!",
    "I am not Quaid!",
    "You think this is the real Quaid? It is!",
    "Sleazy...Demure.",
    "You're Luggage.",
    "Mimetic polyalloy",
    "Sarah Connor?",
];

pub struct VersionInfo {
    app_version: &'static str,
    schema_version: usize,
}

impl Display for VersionInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "arnie version: {app_version}\n\
            arnieplugin schema_version: {schema_version}",
            app_version = self.app_version,
            schema_version = self.schema_version
        )
    }
}

impl VersionInfo {
    pub fn new(app_version: &'static str, schema_version: usize) -> Self {
        Self {
            app_version,
            schema_version,
        }
    }

    /// Gather the real version info and store it
    fn real() -> Self {
        Self {
            app_version: VERSION,
            schema_version: SCHEMA_VERSION,
        }
    }
}

pub struct ArniePlugin<Db: Database> {
    db: DatabaseWrapper<Db>,
    rng: SmallRng,
    version_info: VersionInfo,
}

impl<Db: Database> ArniePlugin<Db> {
    pub async fn new(
        db: DatabaseWrapper<Db>,
        random_seed: u64,
    ) -> anyhow::Result<Self> {
        Self::new_with_version(db, random_seed, VersionInfo::real()).await
    }

    pub async fn new_with_version(
        mut db: DatabaseWrapper<Db>,
        random_seed: u64,
        version_info: VersionInfo,
    ) -> anyhow::Result<Self> {
        // TODO: fail if db schema is later than the code
        if let Ok(Some(current_schema)) = db.get_usize(INSTALLED_KEY).await {
            if current_schema < SCHEMA_VERSION {
                upgrade_schema(current_schema, &mut db).await?;
            }
            // Otherwise, there is nothing to do because we are up to date.
        } else {
            // Arnie is not installed. Do it now
            db.set_usize(INSTALLED_KEY, SCHEMA_VERSION).await?;
            for quote in DEFAULT_QUOTES {
                db.lpush(&quotes_key(ARNIE_NAME), quote).await?;
            }
            db.lpush(PEOPLE_KEY, ARNIE_NAME).await?;
        }

        Ok(Self {
            db,
            rng: SmallRng::seed_from_u64(random_seed),
            version_info,
        })
    }

    async fn random_quote(&mut self, person: &str) -> String {
        // TODO: cache quotes
        let num_quotes = self
            .db
            .llen(&quotes_key(person))
            .await
            .expect("Failed to find quotes!");

        if num_quotes == 0 {
            return format!(
                "(No quotes yet - say !{person} !add \"My quote here\" \
                to add one.)"
            );
        }

        let index = self.rng.gen_range(0..num_quotes);

        self.db
            .lindex(
                &quotes_key(person),
                isize::try_from(index)
                    .expect("Quote index was unexpectedly large!"),
            ) // TODO: make quotes a const
            .await
            .expect("Failed to get quote!")
            .expect("Quote is missing")
    }

    async fn list_quotes(&self, person: &str) -> String {
        self.db
            .lrange(&quotes_key(person), 0, -1)
            .await
            .expect("Failed to get quotes!")
            .iter()
            .enumerate()
            .map(|(i, s)| format!("[{}] {}", i, s))
            .join("\n")
    }

    async fn remove_quote(
        &mut self,
        person: &str,
        index: isize,
    ) -> Result<(), String> {
        let quotes_len =
            self.db.llen(&quotes_key(person)).await.map_err(|_| {
                String::from("Error: failed to find number of quotes")
            })?;

        if index < 0 || index >= quotes_len as isize {
            return Err(Self::error(&format!(
                "Try using a number from 0-{} inclusive.",
                quotes_len - 1
            )));
        }

        let quote = self
            .db
            .lindex(&quotes_key(person), index)
            .await
            .map_err(|_| String::from("Error: couldn't find quote."))?;
        if let Some(quote) = quote {
            self.db
                .lrem(&quotes_key(person), 1, &quote)
                .await
                .map_err(|_| Self::unrecoverable_error("Couldn't remove quote"))
        } else {
            Ok(())
        }
    }

    /// Deletes the quote with index index. (Note: also deletes any other
    /// quotes with value '__deleted__'.)
    pub async fn delete_quote(
        &mut self,
        person: &str,
        index: isize,
    ) -> anyhow::Result<()> {
        self.db
            .lset(&quotes_key(person), index, "__deleted__")
            .await?;
        Ok(self.db.lrem(&quotes_key(person), 0, "__deleted__").await?)
    }

    async fn run_remove(&mut self, person: &str, args_str: &str) -> String {
        let index = args_str.parse();
        if let Ok(index) = index {
            let res = self.remove_quote(person, index).await;
            match res {
                Ok(()) => format!("Quote {}: you have been erased.", index),
                Err(e) => e,
            }
        } else {
            Self::error("Try using a number like '!arnie !remove 3'.")
        }
    }

    async fn run_add(&mut self, person: &str, args_str: &str) -> String {
        let res = self.db.lpush(&quotes_key(person), args_str).await;
        if res.is_ok() {
            String::from("No problemo.")
        } else {
            Self::unrecoverable_error("Couldn't add quote.")
        }
    }

    fn error(resolution: &str) -> String {
        format!("Failure is not an option. {resolution}")
    }

    fn unrecoverable_error(message: &str) -> String {
        format!("I did nothing. The pavement was his enemy. {message}")
    }

    async fn run_person_add(&mut self, name: &str) -> anyhow::Result<String> {
        // Validate the name
        for ch in name.chars() {
            if ch.is_whitespace() {
                return Ok("Error: person names must contain no spaces \
                    or whitespace"
                    .to_owned());
            }
            if ch == '!' || ch == '"' {
                return Ok(
                    r#"Error: person names must not contain '!' or '"'"#
                        .to_owned(),
                );
            }
        }

        // Check whether they already exist
        if self.all_names().await?.iter().any(|n| n == name) {
            return Ok("Error: person already exists!".to_owned());
        }

        self.db.lpush(PEOPLE_KEY, name).await?;

        // Add the person
        Ok(format!("Person {name} added."))
    }

    async fn all_names(&self) -> anyhow::Result<Vec<String>> {
        Ok(self.db.lrange(PEOPLE_KEY, 0, -1).await?)
    }

    async fn run_person_remove(
        &mut self,
        name: &str,
    ) -> anyhow::Result<String> {
        // Delete the person from the person list
        self.db.lrem(PEOPLE_KEY, 0, name).await?;

        // Delete all their quotes
        self.db.del(&quotes_key(name)).await?;

        Ok("Person terminated.".to_owned())
    }

    async fn run_person_list(&self) -> anyhow::Result<String> {
        let people = self.db.lrange(PEOPLE_KEY, 0, -1).await?;

        Ok(people.iter().join(", "))
    }

    async fn process_person_command(
        &mut self,
        command: &HumanCommand,
    ) -> anyhow::Result<String> {
        let subcommand2 = command.keywords.get(2);
        Ok(match subcommand2.map(String::as_str) {
            Some("!add") => self.run_person_add(&command.content()).await?,
            Some("!remove") => {
                self.run_person_remove(&command.content()).await?
            }
            Some("!list") => self.run_person_list().await?,
            Some(_) => "Error: unknown person command, screw you!".to_owned(),
            None => {
                if command.args.is_empty() {
                    self.random_quote(ARNIE_NAME).await
                } else {
                    command.content()
                }
            }
        })
    }

    async fn process_command(
        &mut self,
        command: HumanCommand,
        name: &str,
        name_case: Case,
        room_address: &RoomAddress,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        let person_name = name.to_lowercase();
        let subcommand = command.keywords.get(1);

        let quote = match subcommand.map(String::as_str) {
            Some("!list") => self.list_quotes(&person_name).await,
            Some("!remove") => {
                self.run_remove(&person_name, &command.content()).await
            }
            Some("!add") => {
                self.run_add(&person_name, &command.content()).await
            }
            Some("!version") => self.version_info.to_string(),
            Some("!person") => self.process_person_command(&command).await?,
            Some(_) => "Error: unknown command, screw you!".to_owned(),
            None => {
                if command.args.is_empty() {
                    self.random_quote(&person_name).await
                } else {
                    command.content()
                }
            }
        };

        Ok(Some(OutgoingMessage::new(
            caseify(quote, name_case, &mut self.rng),
            room_address.clone(),
        )))
    }
}

fn caseify(input: String, case: Case, rng: &mut SmallRng) -> String {
    match case {
        Case::Lower => input,
        Case::Title => {
            let mut s = String::with_capacity(input.len());
            let mut prev_was_nonword = true;
            for ch in input.chars() {
                if ch.is_alphabetic() || ch == '\'' {
                    if prev_was_nonword {
                        for c in ch.to_uppercase() {
                            s.push(c);
                        }
                    } else {
                        s.push(ch)
                    };
                    prev_was_nonword = false;
                } else {
                    prev_was_nonword = true;
                    s.push(ch)
                }
            }
            s
        }
        Case::Upper => input.to_uppercase(),
        Case::Mixed => {
            let mut s = String::with_capacity(input.len());
            for ch in input.chars() {
                if rng.gen_bool(0.5) {
                    for c in ch.to_uppercase() {
                        s.push(c);
                    }
                } else {
                    for c in ch.to_lowercase() {
                        s.push(c);
                    }
                }
            }
            s
        }
    }
}

/// Return the key in the database for this person's list of quotes
fn quotes_key(person: &str) -> String {
    format!("{QUOTES_KEY_PREFIX}{person}")
}

async fn upgrade_schema<Db: Database>(
    current_schema: usize,
    db: &mut DatabaseWrapper<Db>,
) -> anyhow::Result<()> {
    for s in current_schema..SCHEMA_VERSION {
        schema_upgrade_from(s, db).await?;
    }
    Ok(())
}

async fn schema_upgrade_from<Db: Database>(
    current_schema: usize,
    db: &mut DatabaseWrapper<Db>,
) -> anyhow::Result<()> {
    match current_schema {
        1 => {
            // Upgrading from 1 to 2
            const V1_QUOTES_KEY: &str = "quotes";

            // Read in old quotes
            let quotes: Vec<String> = db
                .lrange(V1_QUOTES_KEY, 0, -1)
                .await
                .expect("Failed to get quotes!");

            // Write out to new location
            let arnie_key = quotes_key(ARNIE_NAME);
            for quote in quotes {
                db.lpush(&arnie_key, &quote).await?;
            }

            // Delete old quotes key
            db.del(V1_QUOTES_KEY).await?;

            // Add a people list
            db.lpush(PEOPLE_KEY, ARNIE_NAME).await?;

            // Mark the schema as upgraded
            db.set_usize(INSTALLED_KEY, 2).await?;
        }
        _ => panic!("Unexpected previous schema version {current_schema}!"),
    }
    Ok(())
}

#[async_trait]
impl<Db: Database + Send> Plugin for ArniePlugin<Db> {
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        if let Some(parsed) = HumanCommand::parse(&message.text) {
            if let Some(keyword) = parsed.keywords.get(0) {
                let an = self.all_names().await?;
                let (name, name_case) = lower_and_remember(&keyword[1..]);
                if an.iter().any(|n| n.to_lowercase() == name) {
                    return self
                        .process_command(
                            parsed,
                            &name,
                            name_case,
                            &message.room_address,
                        )
                        .await;
                }
            }
        }
        Ok(None)
    }
}

#[derive(Debug, PartialEq)]
enum Case {
    Lower,
    Title,
    Upper,
    Mixed,
}

/// Return the input string converted to lower case, and the case of the
/// unmodified input string.
fn lower_and_remember(input: &str) -> (String, Case) {
    (input.to_lowercase(), find_case(input))
}

fn find_case(input: &str) -> Case {
    let mut first = true;
    let mut first_is_caps = false;
    let mut nonfirst_is_caps = false;
    let mut all_are_caps = true;
    for ch in input.chars() {
        let ch_up = ch.is_uppercase();
        if first {
            first_is_caps = ch_up;
            first = false;
        } else {
            if ch_up {
                nonfirst_is_caps = true;
            }
        }
        if !ch_up {
            all_are_caps = false;
        }
    }

    if all_are_caps {
        Case::Upper
    } else if nonfirst_is_caps {
        Case::Mixed
    } else if first_is_caps {
        Case::Title
    } else {
        Case::Lower
    }
}

#[cfg(test)]
mod test {
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use crate::{memory_database::MemoryDatabase, room_address::RoomAddress};

    use super::*;

    #[test]
    fn lower_and_remember_returns_lc_word_and_what_it_did() {
        assert_eq!(("foo".to_owned(), Case::Lower), lower_and_remember("foo"));
        assert_eq!(("foo".to_owned(), Case::Title), lower_and_remember("Foo"));
        assert_eq!(("foo".to_owned(), Case::Mixed), lower_and_remember("foO"));
        assert_eq!(("foo".to_owned(), Case::Mixed), lower_and_remember("FoO"));
        assert_eq!(("foo".to_owned(), Case::Upper), lower_and_remember("FOO"));
    }

    #[tokio::test]
    async fn arnie_says_different_things_every_time() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask for two messages, they are different
        assert_eq!(
            &quote_response(&mut arnie).await,
            "Consider that a divorce."
        );

        assert_eq!(&quote_response(&mut arnie).await, "It's not a toomah!");
    }

    #[tokio::test]
    async fn allcaps_arnie_says_different_things_every_time() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask for two messages, they are different
        assert_eq!(
            &allcaps_quote_response(&mut arnie).await,
            "CONSIDER THAT A DIVORCE."
        );

        assert_eq!(
            &allcaps_quote_response(&mut arnie).await,
            "IT'S NOT A TOOMAH!"
        );
    }

    #[tokio::test]
    async fn title_case_arnie_says_different_things_every_time() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask for two messages, they are different
        assert_eq!(
            &command_response(&mut arnie, "!Arnie").await,
            "Consider That A Divorce."
        );

        assert_eq!(
            &command_response(&mut arnie, "!Arnie").await,
            "It's Not A Toomah!"
        );
    }

    #[tokio::test]
    async fn arnie_says_what_you_tell_him() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask to say something
        // Then he says it
        assert_eq!(
            &command_response(&mut arnie, "!arnie say this").await,
            "say this"
        );
    }

    #[tokio::test]
    async fn allcaps_arnie_says_what_you_tell_him_in_allcaps() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask to say something
        // Then he says it in all caps
        assert_eq!(
            &command_response(&mut arnie, "!ARNIE say this").await,
            "SAY THIS"
        );
    }

    #[tokio::test]
    async fn title_case_arnie_says_what_you_tell_him_in_title_case() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask to say something
        // Then he says it in title case
        assert_eq!(
            &command_response(&mut arnie, "!Arnie say this").await,
            "Say This"
        );
    }

    #[tokio::test]
    async fn mixed_case_arnie_says_what_you_tell_him_in_crazy_case() {
        // Given an Arnie plugin
        let mut arnie = ArniePlugin::new(new_db(), 6).await.unwrap();

        // When I ask to say something
        // Then he says it in title case
        assert_eq!(
            &command_response(&mut arnie, "!aRnie SAY this").await,
            "saY THiS"
        );
    }

    #[tokio::test]
    async fn allcaps_name_says_what_you_tell_them_in_allcaps() {
        // Given an Arnie plugin with a person added
        let mut arnie = new_arnie().await;
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add jo").await,
            "Person jo added."
        );
        assert_eq!(
            &command_response(&mut arnie, "!jo !add foo").await,
            "No problemo."
        );

        // When I ask to say things
        // Then they say it in the right capitalisation
        assert_eq!(
            &command_response(&mut arnie, "!JO say this").await,
            "SAY THIS"
        );
        assert_eq!(&command_response(&mut arnie, "!JO").await, "FOO");
        assert_eq!(&command_response(&mut arnie, "!Jo").await, "Foo");
        assert_eq!(&command_response(&mut arnie, "!jO").await, "FOo");
    }

    #[tokio::test]
    async fn arnie_list_says_all_the_quotes() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I ask for two messages, they are different
        assert_eq!(
            &command_response(&mut arnie, "!arnie !list").await,
            "\
            [0] You’ve just been erased.\n\
            [1] Let off some steam, Bennett.\n\
            [2] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_first_quote_from_arnie() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I remove the first quote
        assert_eq!(
            &command_response(&mut arnie, "!arnie !remove 0").await,
            "Quote 0: you have been erased."
        );

        // Then it is gone
        assert_eq!(
            &command_response(&mut arnie, "!arnie !list").await,
            "\
            [0] Let off some steam, Bennett.\n\
            [1] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_quotes_from_arnie() {
        // Given an Arnie plugin with lots of quoted
        let mut arnie = new_arnie().await;

        // When I remove a quote with a large index
        assert_eq!(
            &command_response(&mut arnie, "!arnie !remove 15").await,
            "Quote 15: you have been erased."
        );

        // And then remove all but two quotes
        for _ in 0..(DEFAULT_QUOTES.len() - 3) {
            assert_eq!(
                &command_response(&mut arnie, "!arnie !remove 1").await,
                "Quote 1: you have been erased."
            );
        }

        // Then only 2 are left
        assert_eq!(
            &command_response(&mut arnie, "!arnie !list").await,
            "\
            [0] Sarah Connor?\n\
            [1] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_last_quote_from_arnie() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I remove a quote
        assert_eq!(
            &command_response(&mut arnie, "!arnie !remove 2").await,
            "Quote 2: you have been erased."
        );

        // Then it is gone
        assert_eq!(
            &command_response(&mut arnie, "!arnie !list").await,
            "\
            [0] You’ve just been erased.\n\
            [1] Let off some steam, Bennett.\
            "
        );
    }

    #[tokio::test]
    async fn arnie_remove_with_nonnumeric_index_is_an_error() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I remove a quote using a bad index, I see an error
        assert_eq!(
            &command_response(&mut arnie, "!arnie !remove Foo").await,
            "Failure is not an option. Try using a number like '!arnie !remove 3'."
        );
    }

    #[tokio::test]
    async fn arnie_remove_with_wrong_index_is_an_error() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I remove a quote using a negative index, I see an error
        assert_eq!(
            &command_response(&mut arnie, "!arnie !remove -5").await,
            &format!(
                "Failure is not an option. \
                Try using a number from 0-{} inclusive.",
                DEFAULT_QUOTES.len() - 1
            )
        );

        // When I remove a quote using a too-large index, I see an error
        assert_eq!(
            &command_response(&mut arnie, "!arnie !remove 500").await,
            &format!(
                "Failure is not an option. \
                Try using a number from 0-{} inclusive.",
                DEFAULT_QUOTES.len() - 1
            )
        );
    }

    #[tokio::test]
    async fn can_add_a_new_quote() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I add a new quote
        assert_eq!(
            &command_response(&mut arnie, "!arnie !add This is A !NEW quote!")
                .await,
            "No problemo."
        );

        // Then it appears
        assert_eq!(
            &command_response(&mut arnie, "!arnie !list").await,
            "\
            [0] This is A !NEW quote!\n\
            [1] You’ve just been erased.\n\
            [2] Let off some steam, Bennett.\n\
            [3] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_report_version() {
        // Given an Arnie plugin
        let mut arnie =
            new_arnie_with_version(VersionInfo::new("2.31.2", 99)).await;

        // When I ask for the version, he tells me
        assert_eq!(
            &command_response(&mut arnie, "!arnie !version").await,
            "\
            arnie version: 2.31.2\n\
            arnieplugin schema_version: 99\
            "
        );
    }

    #[tokio::test]
    async fn adding_a_person_with_spaces_in_name_is_an_error() {
        let mut arnie = new_arnie().await;

        // When ask to create a person with spaces in their name,
        // Then he objects
        assert_eq!(
            &command_response(&mut arnie, r#"!arnie !person !add "foo bar""#)
                .await,
            "Error: person names must contain no spaces or whitespace"
        );
    }

    #[tokio::test]
    async fn adding_a_person_with_invalid_characters_in_name_is_an_error() {
        let mut arnie = new_arnie().await;

        // When ask to create a person with spaces in their name,
        // Then he objects
        assert_eq!(
            &command_response(&mut arnie, r#"!arnie !person !add "foo!bar""#)
                .await,
            r#"Error: person names must not contain '!' or '"'"#
        );
    }

    #[tokio::test]
    async fn adding_a_person_makes_them_appear_in_the_list() {
        let mut arnie = new_arnie().await;

        // Sanity: only arnie exists at first
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !list").await,
            "arnie"
        );

        // When I add a person
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add jo").await,
            "Person jo added."
        );

        // Then they are added
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !list").await,
            "jo, arnie"
        );
    }

    #[tokio::test]
    async fn asking_for_a_quote_from_someone_with_no_quotes_shows_error() {
        let mut arnie = new_arnie().await;

        // Given a new person with no quotes
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add jo").await,
            "Person jo added."
        );

        // When I ask for a quote, we get told they have none
        assert_eq!(
            &command_response(&mut arnie, "!jo").await,
            r#"(No quotes yet - say !jo !add "My quote here" to add one.)"#
        );
    }

    #[tokio::test]
    async fn can_add_quotes_for_a_new_person() {
        let mut arnie = new_arnie().await;

        // Given I have added a new person
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add neg").await,
            "Person neg added."
        );

        // When I add some quotes for them
        assert_eq!(
            &command_response(&mut arnie, r#"!neg !add "Hi from neg""#).await,
            "No problemo."
        );
        assert_eq!(
            &command_response(&mut arnie, r#"!neg !add "Quote 2""#).await,
            "No problemo."
        );

        // Then they are added
        assert_eq!(
            &command_response(&mut arnie, "!neg !list").await,
            "\
            [0] Quote 2\n\
            [1] Hi from neg\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_quotes_for_a_new_person() {
        let mut arnie = new_arnie().await;

        // Given a new person with quotes
        command_response(&mut arnie, "!arnie !person !add neg").await;
        command_response(&mut arnie, r#"!neg !add "Hi from neg""#).await;
        command_response(&mut arnie, r#"!neg !add "Quote 2""#).await;
        command_response(&mut arnie, r#"!neg !add "Quote 3""#).await;

        // When I remove one
        command_response(&mut arnie, "!neg !remove 1").await;

        // Then it is gone
        assert_eq!(
            &command_response(&mut arnie, "!neg !list").await,
            "\
            [0] Quote 3\n\
            [1] Hi from neg\
            "
        );
    }

    #[tokio::test]
    async fn removing_a_person_makes_them_disappear() {
        let mut arnie = new_arnie().await;

        // Given a few people exist
        command_response(&mut arnie, "!arnie !person !add person1").await;
        command_response(&mut arnie, "!arnie !person !add person2").await;
        command_response(&mut arnie, "!arnie !person !add person3").await;
        command_response(&mut arnie, "!arnie !person !add person4").await;
        command_response(&mut arnie, "!arnie !person !add person5").await;

        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !list").await,
            "person5, person4, person3, person2, person1, arnie"
        );

        // When I remove a person
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !remove person3")
                .await,
            "Person terminated."
        );

        // Then they are added
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !list").await,
            "person5, person4, person2, person1, arnie"
        );
    }

    #[tokio::test]
    async fn removing_and_readding_a_person_deletes_their_quotes() {
        let mut arnie = new_arnie().await;

        // Given I had a person with quotes
        command_response(&mut arnie, "!arnie !person !add nifty").await;
        command_response(&mut arnie, "!nifty !add q1").await;
        command_response(&mut arnie, "!nifty !add q2").await;

        // When I remove them and re-add them
        command_response(&mut arnie, "!arnie !person !remove nifty").await;
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add nifty").await,
            "Person nifty added."
        );

        // Then they have no quotes
        assert_eq!(&command_response(&mut arnie, "!nifty !list").await, "");
    }

    // TODO: if I delete a person and re-add them, their quotes have disappeared

    #[tokio::test]
    async fn adding_a_person_who_already_exists_is_an_error() {
        let mut arnie = new_arnie().await;

        // When I try to add a person with name arnie
        // Then is it an error because he exists by default
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add arnie").await,
            "Error: person already exists!"
        );

        // Given I have added a new person
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add jo").await,
            "Person jo added."
        );

        // When I try to add them again
        // Then it is an error
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !add jo").await,
            "Error: person already exists!"
        );
    }

    #[tokio::test]
    async fn unknown_command_causes_arnie_to_insult_us() {
        let mut arnie = new_arnie().await;

        // When I send a command he doesn't understand, he returns an error.
        assert_eq!(
            &command_response(&mut arnie, "!arnie !knit_socks").await,
            "Error: unknown command, screw you!"
        );
    }

    #[tokio::test]
    async fn unknown_person_command_causes_arnie_to_insult_us() {
        let mut arnie = new_arnie().await;

        // When I send a command he doesn't understand, he returns an error.
        assert_eq!(
            &command_response(&mut arnie, "!arnie !person !knit_socks").await,
            "Error: unknown person command, screw you!"
        );
    }

    #[tokio::test]
    async fn arnie_doesnt_repopulate_his_quotes_if_already_installed() {
        let underlying_db = Arc::new(Mutex::new(MemoryDatabase::new()));
        let db = DatabaseWrapper::new(
            underlying_db.clone(),
            String::from("test_prefix"),
        );

        // Install arnie into a database
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // Delete all his quotes except the first one
        for i in (1..DEFAULT_QUOTES.len()).rev() {
            arnie.delete_quote(ARNIE_NAME, i as isize).await.unwrap();
        }

        // Sanity: he now only ever says one thing
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");

        // Create another instance of arnie on the same same database
        let db =
            DatabaseWrapper::new(underlying_db, String::from("test_prefix"));
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // Check that he didn't recreate all the quotes
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
    }

    #[tokio::test]
    async fn upgrading_1_to_2_brings_across_arnie_quotes() {
        // Given a V1 DB
        let mut db = new_db().namespaced("arnie|");
        db.lpush("quotes", "quote 1").await.unwrap();
        db.lpush("quotes", "quote 2").await.unwrap();
        db.set_usize(INSTALLED_KEY, 1).await.unwrap();

        // When I create an Arnie
        let _arnie = ArniePlugin::new(db.clone(), 4).await.unwrap();

        // Then the DB is V2 and contains the migrated quoted
        assert_eq!(db.get("quotes").await.unwrap(), None);
        assert_eq!(
            db.lrange("person_quotes|arnie", 0, 2).await.unwrap(),
            vec!["quote 1".to_owned(), "quote 2".to_owned()]
        );
    }

    #[tokio::test]
    async fn upgrading_1_to_2_adds_arnie_to_the_people_list() {
        // Given a V1 DB
        let mut db = new_db().namespaced("arnie|");
        db.lpush("quotes", "quote 1").await.unwrap();
        db.lpush("quotes", "quote 2").await.unwrap();
        db.set_usize(INSTALLED_KEY, 1).await.unwrap();

        // When I create an Arnie
        let _arnie = ArniePlugin::new(db.clone(), 4).await.unwrap();

        // Then the DB is V2 and the people list is just arnie
        assert_eq!(db.get("quotes").await.unwrap(), None);
        assert_eq!(
            db.lrange("people", 0, 2).await.unwrap(),
            vec!["arnie".to_owned()]
        );
    }

    #[tokio::test]
    async fn after_upgrading_1_to_2_arnie_still_works() {
        // Given a V1 DB
        let mut db = new_db().namespaced("arnie|");
        db.lpush("quotes", "quote 1").await.unwrap();
        db.lpush("quotes", "quote 2").await.unwrap();
        db.set_usize(INSTALLED_KEY, 1).await.unwrap();

        // When I create an Arnie
        let mut arnie = ArniePlugin::new(db.clone(), 4).await.unwrap();

        // Then arnie still says things
        assert_eq!(&quote_response(&mut arnie).await, "quote 2");
    }

    async fn quote_response(arnie: &mut ArniePlugin<MemoryDatabase>) -> String {
        arnie
            .message(&IncomingMessage::new(
                "!arnie".to_owned(),
                RoomAddress::CommandLine,
                "fake_from".to_owned(),
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }

    async fn allcaps_quote_response(
        arnie: &mut ArniePlugin<MemoryDatabase>,
    ) -> String {
        arnie
            .message(&IncomingMessage::new(
                "!ARNIE".to_owned(),
                RoomAddress::CommandLine,
                "fake_from".to_owned(),
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }

    async fn command_response(
        arnie: &mut ArniePlugin<MemoryDatabase>,
        cmd: &str,
    ) -> String {
        arnie
            .message(&IncomingMessage::new(
                cmd.to_owned(),
                RoomAddress::CommandLine,
                "fake_from".to_owned(),
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }

    async fn new_arnie() -> ArniePlugin<MemoryDatabase> {
        ArniePlugin::new(new_db(), 3).await.unwrap()
    }

    fn new_db() -> DatabaseWrapper<MemoryDatabase> {
        DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        )
    }

    async fn new_arnie_with_version(
        version_info: VersionInfo,
    ) -> ArniePlugin<MemoryDatabase> {
        let db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        );
        ArniePlugin::new_with_version(db, 3, version_info)
            .await
            .unwrap()
    }

    async fn new_3_quote_arnie() -> ArniePlugin<MemoryDatabase> {
        let mut arnie = new_arnie().await;
        for _ in 0..(DEFAULT_QUOTES.len() - 3) {
            arnie.delete_quote(ARNIE_NAME, 0).await.unwrap();
        }
        arnie
    }
}
