use async_trait::async_trait;

use crate::{
    incoming_message::IncomingMessage, outgoing_message::OutgoingMessage,
    room_address::RoomAddress,
};

#[async_trait]
pub trait AdminPlugin {
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<AdminResponse>;
}

#[derive(Debug)]
pub enum AdminAction {
    None,
    Error(String, String),
    Exit(i32),
    MatrixAccessToken,
}

#[derive(Debug)]
pub struct AdminResponse {
    pub action: AdminAction,
    pub message: Option<OutgoingMessage>,
}

impl AdminResponse {
    pub fn none() -> Self {
        Self {
            action: AdminAction::None,
            message: None,
        }
    }

    pub fn error(message: &str, module: &str, error_message: &str) -> Self {
        Self {
            action: AdminAction::Error(
                String::from(module),
                String::from(error_message),
            ),
            message: Some(OutgoingMessage::new(
                message.to_owned(),
                RoomAddress::Admin,
            )),
        }
    }

    pub fn exit(message: &str, exit_code: i32) -> Self {
        Self {
            action: AdminAction::Exit(exit_code),
            message: Some(OutgoingMessage::new(
                message.to_owned(),
                RoomAddress::Admin,
            )),
        }
    }

    pub(crate) fn matrix_access_token() -> Self {
        Self {
            action: AdminAction::MatrixAccessToken,
            message: None,
        }
    }
}
