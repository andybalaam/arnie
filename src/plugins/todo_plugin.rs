use async_trait::async_trait;
use itertools::Itertools;

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::human_command::HumanCommand;
use crate::incoming_message::IncomingMessage;
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::plugin::Plugin;
use crate::room_address::RoomAddress;

const TODOS_KEY: &str = "todos";

pub struct TodoPlugin<Db>
where
    Db: Database,
{
    db: DatabaseWrapper<Db>,
}

impl<Db> TodoPlugin<Db>
where
    Db: Database,
{
    pub fn new(db: DatabaseWrapper<Db>) -> Self {
        Self { db }
    }
}

impl<Db> TodoPlugin<Db>
where
    Db: Database,
{
    async fn process_command(
        &mut self,
        command: HumanCommand,
        room_address: &RoomAddress,
        sender: &str,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        let top_command: Option<&str> = command.keywords.get(0).map(|s| &**s);
        let sub_command: Option<&str> = command.keywords.get(1).map(|s| &**s);

        let db_paths = DbPaths::new(sender);

        let response = match (top_command.as_deref(), sub_command.as_deref()) {
            (Some("!todo"), Some("!help")) => Some(self.help()),
            (Some("!todo"), None) => {
                Some(self.todo(&db_paths, command.content()).await)
            }
            (Some("!todos"), None) => Some(self.todos(&db_paths).await),
            (Some("!dones"), None) => Some(self.dones(&db_paths).await),
            (Some("!wontdo"), None) => {
                Some(self.wontdo(&db_paths, &command.args).await)
            }
            (Some("!done"), None) => {
                Some(self.done(&db_paths, &command.args).await)
            }
            (Some("!cleardone"), None) => Some(self.cleardone(&db_paths).await),
            _ => Some(self.unknown_subcommand()),
        };

        let response = response.transpose();

        response.map(|opt| {
            opt.map(|msg| OutgoingMessage::new(msg, room_address.clone()))
        })
    }

    async fn todos(&mut self, db_paths: &DbPaths) -> anyhow::Result<String> {
        let items = self.db.hgetall(&db_paths.todo()).await?;

        if items.is_empty() {
            Ok("No tasks found.".to_owned())
        } else {
            let mut items_vec = items.into_iter().collect_vec();
            items_vec.sort_by_key(|i| i.0.parse::<usize>().unwrap_or(0));
            Ok(items_vec
                .iter()
                .map(|(i, s)| format!("[{i}]: {s}"))
                .join("\n"))
        }
    }

    async fn dones(&mut self, db_paths: &DbPaths) -> anyhow::Result<String> {
        let items = self.db.hgetall(&db_paths.done()).await?;

        if items.is_empty() {
            Ok("No tasks found.".to_owned())
        } else {
            let mut items_vec = items.into_iter().collect_vec();
            items_vec.sort_by_key(|i| i.0.parse::<usize>().unwrap_or(0));
            Ok(items_vec
                .iter()
                .map(|(i, s)| format!("[{i}]: {s}"))
                .join("\n"))
        }
    }

    async fn wontdo(
        &mut self,
        db_paths: &DbPaths,
        ids: &Vec<String>,
    ) -> anyhow::Result<String> {
        // TODO: check tasks exist
        for id in ids {
            self.db.hdel(&db_paths.todo(), id).await?;
        }
        if ids.len() == 1 {
            Ok("Task eliminated.".to_owned())
        } else {
            Ok("Tasks eliminated.".to_owned())
        }
    }

    async fn todo(
        &mut self,
        db_paths: &DbPaths,
        content: String,
    ) -> anyhow::Result<String> {
        // TODO: this has a race - someone else could get and update the
        // ID while we are doing it. It's not that big a deal because
        // most people won't be adding multiple tasks to their own task list
        // simultaneously.
        let id = self.db.get_usize(&db_paths.next_id()).await?.unwrap_or(0) + 1;
        self.db.set_usize(&db_paths.next_id(), id).await?;

        let id = format!("{id:0>4}");

        self.db.hset(&db_paths.todo(), &id, &content).await?;

        Ok(format!("Task {id} added."))
    }

    async fn done(
        &mut self,
        db_paths: &DbPaths,
        ids: &Vec<String>,
    ) -> anyhow::Result<String> {
        // TODO: check tasks exist
        for id in ids {
            let existing = self.db.hget(&db_paths.todo(), id).await?;
            if let Some(existing) = existing {
                self.db.hset(&db_paths.done(), id, &existing).await?;
                self.db.hdel(&db_paths.todo(), id).await?;
            }
        }
        if ids.len() == 1 {
            Ok("Hasta la vista, baby.".to_owned())
        } else {
            Ok("Hasta la vista, babies.".to_owned())
        }
    }

    async fn cleardone(
        &mut self,
        db_paths: &DbPaths,
    ) -> anyhow::Result<String> {
        self.db.del(&db_paths.done()).await?;
        Ok("All done items cleared.".to_owned())
    }

    fn help(&mut self) -> anyhow::Result<String> {
        Ok("\
            !todo <task>\n\
            Add something you need to do\n\
            \n\
            !done <task_num>\n\
            Say you did something\n\
            \n\
            !wontdo <task_num>\n\
            Say you won't do something (remove it)\n\
            \n\
            !todos \n\
            List the things you need to do\n\
            \n\
            !dones \n\
            List the things you have done\n\
            \n\
            !cleardone \n\
            Forget all the things you have done\n\
            \n\
            !todo !help\n\
            Display this help message.\n\
            \n\
            Examples:\n\
            \n\
            !todo Feed the cat\n\
            !todos\n\
            >>> [0] Feed the cat
            !done 0\n\
        "
        .to_owned())
    }

    fn unknown_subcommand(&mut self) -> anyhow::Result<String> {
        Ok("Error: unknown subcommand. Try !todo !help.".to_owned())
    }
}

#[async_trait]
impl<Db> Plugin for TodoPlugin<Db>
where
    Db: Database + Send,
{
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        if let Some(parsed) = HumanCommand::parse(&message.text) {
            if let Some(keyword) = parsed.keywords.get(0) {
                if keyword == "!todo"
                    || keyword == "!done"
                    || keyword == "!todos"
                    || keyword == "!wontdo"
                    || keyword == "!dones"
                    || keyword == "!cleardone"
                {
                    return self
                        .process_command(
                            parsed,
                            &message.room_address,
                            message.from.id(),
                        )
                        .await;
                }
            }
        }
        Ok(None)
    }
}

struct DbPaths {
    sender: String,
}

impl DbPaths {
    fn new(sender: &str) -> Self {
        Self {
            sender: sender.to_owned(),
        }
    }

    fn next_id(&self) -> String {
        format!("{TODOS_KEY}|{}|next_id", self.sender)
    }

    fn todo(&self) -> String {
        format!("{TODOS_KEY}|{}|todo", self.sender)
    }

    fn done(&self) -> String {
        format!("{TODOS_KEY}|{}|done", self.sender)
    }
}

#[cfg(test)]
mod test {
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use crate::memory_database::MemoryDatabase;

    use super::*;

    #[tokio::test]
    async fn can_add_and_list_todos() {
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Given we added some todos to the DB
        {
            let mut todo = TodoPlugin::new(DatabaseWrapper::new(
                db.clone(),
                "".to_owned(),
            ));

            assert_eq!(
                todo.test_m("!todo Buy socks").await,
                "Task 0001 added."
            );
            assert_eq!(
                todo.test_m("!todo Put out washing").await,
                "Task 0002 added."
            );
        }

        // When make a new plugin based on the same DB
        // And ask it for the todos
        // Then it lists them
        let mut todo2 =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));

        assert_eq!(
            todo2.test_m("!todos").await,
            "[0001]: Buy socks\n[0002]: Put out washing"
        );
    }

    #[tokio::test]
    async fn an_unknown_subcommand_is_an_error() {
        // Given a TODO plugin
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));
        let mut todo =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));

        // When I provide a wrong subcommand
        // Then Arnie tells us we are wrong
        assert_eq!(
            todo.test_m("!todo !unknown").await,
            "Error: unknown subcommand. Try !todo !help."
        );

        // And Arnie keeps working
        assert_eq!(
            todo.test_m("!todo !unknown2").await,
            "Error: unknown subcommand. Try !todo !help."
        );
    }

    #[tokio::test]
    async fn marking_an_item_as_wontdo_removes_it_from_the_list() {
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Given we added some todos to the DB
        let mut todo =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));

        assert_eq!(todo.test_m("!todo Buy socks").await, "Task 0001 added.");
        assert_eq!(
            todo.test_m("!todo Put out washing").await,
            "Task 0002 added."
        );
        assert_eq!(todo.test_m("!todo Shave cat").await, "Task 0003 added.");

        // When we mark one as wontdo
        assert_eq!(todo.test_m("!wontdo 0002").await, "Task eliminated.");

        // Then it disappears from the list
        assert_eq!(
            todo.test_m("!todos").await,
            "[0001]: Buy socks\n[0003]: Shave cat"
        );

        // And when we mark multiple as wontdo
        assert_eq!(todo.test_m("!wontdo 0001 0003").await, "Tasks eliminated.");

        // Then they all disappear from the list
        assert_eq!(todo.test_m("!todos").await, "No tasks found.");
    }

    #[tokio::test]
    async fn marking_an_item_as_done_removes_from_todo_and_adds_to_dones() {
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Given we added some todos to the DB
        let mut todo =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));

        assert_eq!(todo.test_m("!todo Buy socks").await, "Task 0001 added.");
        assert_eq!(
            todo.test_m("!todo Put out washing").await,
            "Task 0002 added."
        );
        assert_eq!(todo.test_m("!todo Shave cat").await, "Task 0003 added.");
        assert_eq!(todo.test_m("!todo Pair socks").await, "Task 0004 added.");

        // When we mark one as done
        assert_eq!(todo.test_m("!done 0002").await, "Hasta la vista, baby.");

        // Then it disappears from the todo list
        assert_eq!(
            todo.test_m("!todos").await,
            "[0001]: Buy socks\n[0003]: Shave cat\n[0004]: Pair socks"
        );

        // And it appears in the dones list
        assert_eq!(todo.test_m("!dones").await, "[0002]: Put out washing");

        // And when we mark multiple as done
        assert_eq!(
            todo.test_m("!done 0003 0001").await,
            "Hasta la vista, babies."
        );

        // Then they should all disappear from the todo list
        assert_eq!(todo.test_m("!todos").await, "[0004]: Pair socks");

        // And it appears in the dones list
        assert_eq!(
            todo.test_m("!dones").await,
            "[0001]: Buy socks\n[0002]: Put out washing\n[0003]: Shave cat"
        );
    }

    #[tokio::test]
    async fn clearing_done_list_makes_dones_empty() {
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Given we added some todos to the DB and did 2 of them
        let mut todo =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));

        assert_eq!(todo.test_m("!todo Buy socks").await, "Task 0001 added.");
        assert_eq!(todo.test_m("!todo Shave cat").await, "Task 0002 added.");
        assert_eq!(todo.test_m("!todo Pair socks").await, "Task 0003 added.");
        assert_eq!(todo.test_m("!todo Snip socks").await, "Task 0004 added.");
        assert_eq!(todo.test_m("!done 0001").await, "Hasta la vista, baby.");
        assert_eq!(todo.test_m("!done 0003").await, "Hasta la vista, baby.");

        // When we clear dones
        assert_eq!(todo.test_m("!cleardone").await, "All done items cleared.");

        // Then they are gone from the done list
        assert_eq!(todo.test_m("!dones").await, "No tasks found.");

        // And when we mark another as done
        assert_eq!(todo.test_m("!done 0002").await, "Hasta la vista, baby.");

        // Then it appears in the dones list
        assert_eq!(todo.test_m("!dones").await, "[0002]: Shave cat");

        // And the todos list is untouched
        assert_eq!(todo.test_m("!todos").await, "[0004]: Snip socks");
    }

    #[tokio::test]
    async fn tasks_are_sorted_numerically() {
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Given we have lots of todos in the DB
        let mut todo =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));
        for i in 1..=10000 {
            assert_eq!(
                todo.test_m(&format!("!todo t {i:0>4}")).await,
                format!("Task {i:0>4} added.")
            );
        }

        // Then it disappers from the list
        let todos = todo.test_m("!todos").await;
        let todos: Vec<_> = todos.split('\n').collect();
        assert_eq!(todos[0], "[0001]: t 0001");
        assert_eq!(todos[1], "[0002]: t 0002");
        assert_eq!(todos[9999], "[10000]: t 10000");
    }

    #[tokio::test]
    async fn different_users_have_independent_lists() {
        let db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Given we added some todos to the DB
        let mut todo =
            TodoPlugin::new(DatabaseWrapper::new(db.clone(), "".to_owned()));

        assert_eq!(
            todo.test_m2("!todo Buy socks", "Hopeful Harry").await,
            "Task 0001 added."
        );
        assert_eq!(
            todo.test_m2("!todo Put out washing", "Curious Mo").await,
            "Task 0001 added."
        );

        // When I list the items for each person
        // Then they have separate lists
        assert_eq!(
            todo.test_m2("!todos", "Hopeful Harry").await,
            "[0001]: Buy socks"
        );
        assert_eq!(
            todo.test_m2("!todos", "Curious Mo").await,
            "[0001]: Put out washing"
        );

        // And another user has no items
        assert_eq!(
            todo.test_m2("!todos", "Anxious Andy").await,
            "No tasks found."
        );
    }

    impl<Db: Database + Send> TodoPlugin<Db> {
        async fn test_m(&mut self, text: &str) -> String {
            self.test_m2(text, "fake_from").await
        }

        async fn test_m2(&mut self, text: &str, sender: &str) -> String {
            self.message(&IncomingMessage::new(
                text.to_owned(),
                RoomAddress::CommandLine,
                sender.to_owned(),
            ))
            .await
            .expect("Error from message()")
            .expect("Empty response from message")
            .text
        }
    }
}
