use async_trait::async_trait;

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::human_command::HumanCommand;
use crate::incoming_message::{IncomingMessage, SenderAddress};
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::plugin::Plugin;

const SCHEMA_VERSION: usize = 1;

pub struct GoldstarPlugin<Db: Database> {
    db: DatabaseWrapper<Db>,
}

impl<Db: Database> GoldstarPlugin<Db> {
    pub async fn new(mut db: DatabaseWrapper<Db>) -> anyhow::Result<Self> {
        // TODO: fail if db schema is later than the code
        if !db.exists("installed").await? {
            db.set_usize("installed", SCHEMA_VERSION).await?;
        }

        Ok(Self { db })
    }

    async fn do_goldstar(
        &mut self,
        sender: &dyn SenderAddress,
        content: String,
    ) -> anyhow::Result<String> {
        // TODO prevent races by writing self.db.pipe().set() etc.

        let (name, reason) = find_name_and_reason(&content);

        if name.is_empty() {
            return Ok(
                "Error: you must say who the gold star is for!".to_owned()
            );
        }

        if sender.canonically_equals(name) {
            return Ok(
                "You are not allowed to goldstar yourself! Cheeky!".to_owned()
            );
        }

        let name = sender.canonicalize(name);

        let total = self.db.incr(&format!("goldstar|{name}"), 1).await?;

        let maybe_plural = if total == 1 { "" } else { "s" };

        let msg = if reason.is_empty() {
            format!("Well done {name}! You now have {total} gold star{maybe_plural}!")
        } else {
            format!("{name}: {reason}! You now have {total} gold star{maybe_plural}!")
        };

        Ok(msg)
    }
}

fn find_name_and_reason(content: &str) -> (&str, &str) {
    content
        .split_once(char::is_whitespace)
        .unwrap_or((content, ""))
}

#[async_trait]
impl<Db: Database + Send> Plugin for GoldstarPlugin<Db> {
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        let parsed = HumanCommand::parse(&message.text);
        if let Some(command) = parsed {
            if let Some(keyword) = command.keywords.get(0) {
                if keyword == "!goldstar" {
                    let text = self
                        .do_goldstar(message.from.as_ref(), command.content())
                        .await?;
                    return Ok(Some(OutgoingMessage {
                        text,
                        room_address: message.room_address.clone(),
                    }));
                }
            }
        }
        Ok(None)
    }
}

#[cfg(test)]
mod test {

    use std::sync::Arc;

    use tokio::sync::Mutex;

    use crate::{memory_database::MemoryDatabase, room_address::RoomAddress};
    use matrix_sdk::ruma::OwnedUserId;

    use super::*;

    #[tokio::test]
    async fn can_award_someone_a_gold_star() {
        let mut goldstar = new_goldstar().await;

        assert_eq!(
            command_response(&mut goldstar, "!goldstar andyb").await,
            "Well done andyb! You now have 1 gold star!"
        );
    }

    #[tokio::test]
    async fn ignores_commands_that_dont_start_with_goldstar() {
        let mut goldstar = new_goldstar().await;

        // Manually send message since we expect the output to be None
        let response = goldstar
            .message(&IncomingMessage::new(
                "!arnie".to_owned(),
                RoomAddress::CommandLine,
                "clarke".to_owned(),
            ))
            .await
            .unwrap();

        assert!(response.is_none());
    }

    #[tokio::test]
    async fn ignores_non_command_text() {
        let mut goldstar = new_goldstar().await;

        // Manually send message since we expect the output to be None
        let response = goldstar
            .message(&IncomingMessage::new(
                "Something normal".to_owned(),
                RoomAddress::CommandLine,
                "clarke".to_owned(),
            ))
            .await
            .unwrap();

        assert!(response.is_none());
    }

    #[tokio::test]
    async fn counts_up_gold_stars_as_they_are_awarded() {
        let mut goldstar = new_goldstar().await;

        // Award one goldstar
        command_response(&mut goldstar, "!goldstar andyc").await;

        // Award a second
        assert_eq!(
            command_response(&mut goldstar, "!goldstar andyc").await,
            "Well done andyc! You now have 2 gold stars!"
        );
    }

    #[tokio::test]
    async fn gold_star_counts_for_different_people_are_independent() {
        let mut goldstar = new_goldstar().await;

        // andyb has 2 gold stars
        command_response(&mut goldstar, "!goldstar andyb").await;
        command_response(&mut goldstar, "!goldstar andyb").await;

        // andyc has 3
        command_response(&mut goldstar, "!goldstar andyc").await;
        command_response(&mut goldstar, "!goldstar andyc").await;
        command_response(&mut goldstar, "!goldstar andyc").await;

        // Give andyb 1 one more
        assert_eq!(
            command_response(&mut goldstar, "!goldstar andyb").await,
            "Well done andyb! You now have 3 gold stars!"
        );

        // Give andyc 1 one more
        assert_eq!(
            command_response(&mut goldstar, "!goldstar andyc").await,
            "Well done andyc! You now have 4 gold stars!"
        );
    }

    #[tokio::test]
    async fn remembers_how_many_gold_stars_you_have_after_restart() {
        let mem_db = Arc::new(Mutex::new(MemoryDatabase::new()));

        // Our first plugin saved some info to the DB
        {
            let db1 = DatabaseWrapper::new(
                mem_db.clone(),
                String::from("test_prefix"),
            );

            let mut goldstar1 = new_goldstar_with_db(db1).await;

            command_response(&mut goldstar1, "!goldstar andyb").await;
            command_response(&mut goldstar1, "!goldstar andyb").await;
        }

        // A second plugin can see it
        let db2 = DatabaseWrapper::new(mem_db, String::from("test_prefix"));
        let mut goldstar2 = new_goldstar_with_db(db2).await;
        assert_eq!(
            command_response(&mut goldstar2, "!goldstar andyb").await,
            "Well done andyb! You now have 3 gold stars!"
        );
    }

    #[tokio::test]
    async fn cant_give_goldstar_to_empty_string() {
        let mut goldstar = new_goldstar().await;
        assert_eq!(
            command_response(&mut goldstar, "!goldstar").await,
            "Error: you must say who the gold star is for!"
        );
    }

    #[tokio::test]
    async fn can_give_a_reason_for_a_gold_star() {
        let mut goldstar = new_goldstar().await;
        assert_eq!(
            command_response(
                &mut goldstar,
                "!goldstar mrwsl Good work attending today's stream"
            )
            .await,
            "mrwsl: Good work attending today's stream! \
            You now have 1 gold star!"
        );

        assert_eq!(
            command_response(
                &mut goldstar,
                "!goldstar mrwsl Thanks for staying to the end"
            )
            .await,
            "mrwsl: Thanks for staying to the end! \
            You now have 2 gold stars!"
        );
    }

    #[tokio::test]
    async fn cant_give_yourself_a_gold_star() {
        // Given a plugin
        let mut goldstar = new_goldstar().await;

        // Then you can't goldstar yourself
        assert_eq!(
            command_response_with_from(&mut goldstar, "!goldstar andy", "andy")
                .await,
            "You are not allowed to goldstar yourself! Cheeky!"
        );

        // But you can goldstar other people
        assert_eq!(
            command_response_with_from(
                &mut goldstar,
                "!goldstar crabby",
                "andy"
            )
            .await,
            "Well done crabby! You now have 1 gold star!"
        );
    }

    #[tokio::test]
    async fn gold_starring_someones_matrix_id_strips_server_and_at() {
        let mut goldstar = new_goldstar().await;

        // Give andyb 1 one more
        assert_eq!(
            command_response_with_from(
                &mut goldstar,
                "!goldstar @andyb:example.com",
                OwnedUserId::try_from("@another:matrix.com").unwrap()
            )
            .await,
            "Well done andyb! You now have 1 gold star!"
        );
    }

    #[tokio::test]
    async fn cant_give_yourself_a_gold_star_even_by_matrix_id() {
        // Given a plugin
        let mut goldstar = new_goldstar().await;

        // Then you can't goldstar yourself
        assert_eq!(
            command_response_with_from(
                &mut goldstar,
                "!goldstar @andy:example.com",
                OwnedUserId::try_from("@andy:example.com").unwrap()
            )
            .await,
            "You are not allowed to goldstar yourself! Cheeky!"
        );

        // But you can goldstar other people
        assert_eq!(
            command_response_with_from(
                &mut goldstar,
                "!goldstar @crabby:example.com",
                OwnedUserId::try_from("@andy:example.com").unwrap()
            )
            .await,
            "Well done crabby! You now have 1 gold star!"
        );
    }

    // same as above for blames

    async fn command_response(
        goldstar: &mut GoldstarPlugin<MemoryDatabase>,
        command: &str,
    ) -> String {
        command_response_with_from(goldstar, command, "fake_from").await
    }

    async fn command_response_with_from(
        goldstar: &mut GoldstarPlugin<MemoryDatabase>,
        command: &str,
        from: impl SenderAddress + 'static,
    ) -> String {
        goldstar
            .message(&IncomingMessage::new(
                command.to_owned(),
                RoomAddress::CommandLine,
                from,
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }

    async fn new_goldstar() -> GoldstarPlugin<MemoryDatabase> {
        let db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        );
        new_goldstar_with_db(db).await
    }

    async fn new_goldstar_with_db(
        db: DatabaseWrapper<MemoryDatabase>,
    ) -> GoldstarPlugin<MemoryDatabase> {
        GoldstarPlugin::new(db).await.unwrap()
    }
}
