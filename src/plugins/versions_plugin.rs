use std::collections::HashMap;
use std::fmt::{Display, Formatter, Write};
use std::future::Future;
use std::pin::Pin;

use anyhow::Context;
use async_trait::async_trait;
use matrix_sdk::reqwest;
use regex::Regex;
use reqwest::Url;
use thiserror::Error;
use url::ParseError;

use crate::human_command::HumanCommand;
use crate::incoming_message::IncomingMessage;
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::plugin::Plugin;
use crate::room_address::RoomAddress;

const INTERESTING_PACKAGE_GROUPS: &[&[&str]] = &[&[
    "matrix-rust-sdk-crypto-wasm",
    "matrix-sdk-common",
    "matrix-sdk-crypto",
]];

struct RustUrls {
    lock: Url,
    toml: Option<Url>,
}

enum SubstitutedUrls {
    Rust(RustUrls),
}

/// The URLs needed to find dependency files for a given version of a package
enum PackageUrls {
    Rust {
        /// The location of the Cargo.lock file if we know the version of the
        /// package
        release_lock: String,

        /// The location of the Cargo.lock file if we know the commit id of the
        /// package
        commit_lock: String,

        /// If the supplied lock files are for a workspace, this must be the
        /// location of the Cargo.toml file for the specific crate
        /// within that workspace, if we know the version of
        /// the package
        release_toml: Option<String>,

        /// If the supplied lock files are for a workspace, this must be the
        /// location of the Cargo.toml file for the specific crate
        /// within that workspace, if we know the commit id
        /// of the package
        commit_toml: Option<String>,
    },
    // TODO: e.g. NPM
}

impl PackageUrls {
    fn substitute(
        &self,
        project_info: &ProjectInfo,
    ) -> Result<SubstitutedUrls, ParseError> {
        let project = &project_info.name;
        Ok(match self {
            PackageUrls::Rust {
                release_lock,
                commit_lock,
                release_toml,
                commit_toml,
            } => SubstitutedUrls::Rust(match &project_info.version {
                ProjectVersion::Release(r) => RustUrls {
                    lock: substitute_release(release_lock, project, r)?,
                    toml: match release_toml {
                        Some(u) => Some(substitute_release(&u, project, r)?),
                        None => None,
                    },
                },
                ProjectVersion::Commit(c)
                | ProjectVersion::CommitBranch(c, _) => RustUrls {
                    lock: substitute_commit(commit_lock, project, c)?,
                    toml: match commit_toml {
                        Some(u) => Some(substitute_commit(&u, project, c)?),
                        None => None,
                    },
                },
            }),
        })
    }
}

pub trait HttpClient: Send + Sync {
    fn get(
        &self,
        url: &Url,
    ) -> Pin<Box<dyn Future<Output = Result<String, HttpError>> + Send>>;
}

pub struct VersionsPlugin<H: HttpClient> {
    http_client: H,
    packages: HashMap<String, PackageUrls>,
}

#[derive(Error, Debug)]
pub enum CreateVersionsError {
    #[error("failed to build client")]
    ReqwestClientBuildError(#[from] reqwest::Error),
}

impl VersionsPlugin<reqwest::Client> {
    pub fn new_real() -> Result<Self, CreateVersionsError> {
        let packages = [
            (
                "matrix-rust-sdk-crypto-wasm".to_owned(),
                PackageUrls::Rust {
                    release_lock:
                        "https://raw.githubusercontent.com/matrix-org/\
                        {project}/refs/tags/{version}/Cargo.lock"
                            .to_owned(),
                    commit_lock:
                        "https://raw.githubusercontent.com/matrix-org/\
                        {project}/{commit}/Cargo.lock"
                            .to_owned(),
                    release_toml: None,
                    commit_toml: None,
                },
            ),
            (
                "matrix-sdk-common".to_owned(),
                PackageUrls::Rust {
                    release_lock:
                        "https://raw.githubusercontent.com/matrix-org/\
                        matrix-rust-sdk/refs/tags/{project}-{release}/\
                        Cargo.lock"
                            .to_owned(),
                    commit_lock:
                        "https://raw.githubusercontent.com/matrix-org/\
                        matrix-rust-sdk/{commit}/Cargo.lock"
                            .to_owned(),
                    release_toml: Some(
                        "https://raw.githubusercontent.com/matrix-org/\
                        matrix-rust-sdk/refs/tags/{project}-{release}/\
                        crates/{project}/Cargo.toml"
                            .to_owned(),
                    ),
                    commit_toml: Some(
                        "https://raw.githubusercontent.com/matrix-org/\
                        matrix-rust-sdk/{commit}/\
                        crates/{project}/Cargo.toml"
                            .to_owned(),
                    ),
                },
            ),
        ]
        .into();

        Ok(Self {
            http_client: reqwest::ClientBuilder::new().build()?,
            packages,
        })
    }
}

#[derive(Debug, PartialEq)]
struct DepTree {
    name: String,
    version: ProjectVersion,
    deps: Vec<DepTree>,
}

impl DepTree {
    fn new(name: String, version: ProjectVersion) -> Self {
        Self {
            name,
            version,
            deps: Vec::new(),
        }
    }

    #[cfg(test)]
    fn with_release(name: &str, release: &str) -> Self {
        Self {
            name: name.to_owned(),
            version: ProjectVersion::Release(release.to_owned()),
            deps: Vec::new(),
        }
    }

    #[cfg(test)]
    fn with_commit(name: &str, commit: &str) -> Self {
        Self {
            name: name.to_owned(),
            version: ProjectVersion::Commit(commit.to_owned()),
            deps: Vec::new(),
        }
    }

    #[cfg(test)]
    fn with_commit_branch(name: &str, commit: &str, branch: &str) -> Self {
        Self {
            name: name.to_owned(),
            version: ProjectVersion::CommitBranch(
                commit.to_owned(),
                branch.to_owned(),
            ),
            deps: Vec::new(),
        }
    }

    fn fmt_depth(
        &self,
        depth: usize,
        f: &mut Formatter<'_>,
    ) -> std::fmt::Result {
        for _ in 0..depth {
            f.write_str("... ")?;
        }
        f.write_str(&self.name)?;
        f.write_char(' ')?;
        self.version.fmt(f)?;
        f.write_char('\n')?;

        for child in &self.deps {
            child.fmt_depth(depth + 1, f)?;
        }

        Ok(())
    }
}

impl Display for DepTree {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.fmt_depth(0, f)
    }
}

fn interesting_group(project: &str) -> Option<&&[&str]> {
    INTERESTING_PACKAGE_GROUPS
        .iter()
        .find(|&group| group.contains(&project))
}

impl<H: HttpClient> VersionsPlugin<H> {
    async fn process_command(
        &mut self,
        command: HumanCommand,
        room_address: &RoomAddress,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        let project_info = ProjectInfo::from_args(&command.args)
            .context("Please supply project name and version")?;

        let package =
            self.packages.get(&project_info.name).ok_or_else(|| {
                anyhow::anyhow!(
                    "I don't know anything about package '{}'",
                    project_info.name
                )
            })?;

        let interesting_packages = interesting_group(&project_info.name)
            .ok_or_else(|| {
                anyhow::anyhow!(
                    "Project '{}' is not in any of our interesting groups",
                    &project_info.name
                )
            })?;

        let mut dependencies = DepTree::new(
            project_info.name.clone(),
            project_info.version.clone(),
        );

        let urls: SubstitutedUrls = package.substitute(&project_info)?;
        match urls {
            SubstitutedUrls::Rust(rust_urls) => {
                if rust_urls.toml.is_some() {
                    panic!("We don't support multi-project workspaces yet!");
                }

                self.fetch_and_parse(
                    &rust_urls.lock,
                    interesting_packages,
                    &mut dependencies,
                )
                .await;
            }
        }

        Ok(Some(OutgoingMessage::new(
            dependencies.to_string(),
            room_address.clone(),
        )))
    }

    async fn fetch_and_parse(
        &self,
        url: &Url,
        interesting_packages: &[&str],
        dependencies: &mut DepTree,
    ) {
        let response = self.http_client.get(url).await;
        match response {
            Ok(response) => {
                parse_response(interesting_packages, dependencies, &response);
            }
            Err(e) => match e {
                HttpError::NotFound => {}
                HttpError::ReqwestError(error) => {
                    println!("Error making request {}: {}", url, error)
                }
            },
        };
    }
}
fn parse_response(
    interesting_packages: &[&str],
    parent: &mut DepTree,
    response: &str,
) {
    match response.parse::<toml::Table>() {
        Ok(t) => {
            if let Some(packages) =
                t.get("package").and_then(toml::Value::as_array)
            {
                for package in packages {
                    if let Some(package) = package.as_table() {
                        parse_response_package(
                            interesting_packages,
                            parent,
                            package,
                        );
                    }
                }
            }
        }
        Err(_e) => {
            // It's not toml: that is fine.
        }
    }
}

fn parse_response_package(
    interesting_packages: &[&str],
    parent: &mut DepTree,
    package: &toml::map::Map<String, toml::Value>,
) {
    let Some(name) = toml_prop_str(package, "name") else {
        return;
    };

    if !interesting_packages.contains(&name) {
        return;
    }

    let Some(version) = toml_prop_str(package, "source")
        .and_then(|s| source_version(s))
        .or_else(|| toml_prop_str(package, "version").map(From::from))
    else {
        return;
    };

    let dep = DepTree::new(name.to_owned(), version);
    parent.deps.push(dep);
}

fn source_version(source: &str) -> Option<ProjectVersion> {
    let branch_commit_re =
        Regex::new(r"\?branch=([^#]+)#([0-9a-f]{8})[0-9a-f]{32}$").unwrap();
    if let Some(c) = branch_commit_re.captures(source) {
        return Some(ProjectVersion::CommitBranch(
            c.get(2).unwrap().as_str().to_owned(),
            c.get(1).unwrap().as_str().to_owned(),
        ));
    }

    let commit_re = Regex::new(r"#([0-9a-f]{8})[0-9a-f]{32}$").unwrap();
    if let Some(c) = commit_re.captures(source) {
        return Some(ProjectVersion::Commit(
            c.get(1).unwrap().as_str().to_owned(),
        ));
    }

    // TODO: release versions
    None
}

fn toml_prop_str<'a>(
    value: &'a toml::map::Map<String, toml::Value>,
    property: &'a str,
) -> Option<&'a str> {
    value.get(property).and_then(toml::Value::as_str)
}

#[async_trait]
impl<H: HttpClient> Plugin for VersionsPlugin<H> {
    async fn message(
        &mut self,
        message: &IncomingMessage,
    ) -> anyhow::Result<Option<OutgoingMessage>> {
        if let Some(parsed) = HumanCommand::parse(&message.text) {
            if let Some(keyword) = parsed.keywords.first() {
                if keyword == "!versions" {
                    return self
                        .process_command(parsed, &message.room_address)
                        .await;
                }
            }
        }
        Ok(None)
    }
}

#[derive(Error, Debug)]
pub enum HttpError {
    #[error("Not found")]
    NotFound,
    #[error("HTTP request failed")]
    ReqwestError(reqwest::Error),
}

impl From<reqwest::Error> for HttpError {
    fn from(error: reqwest::Error) -> Self {
        if let Some(status) = error.status() {
            if status == 404 {
                return HttpError::NotFound;
            }
        }
        HttpError::ReqwestError(error)
    }
}

impl HttpClient for reqwest::Client {
    fn get(
        &self,
        url: &Url,
    ) -> Pin<Box<dyn Future<Output = Result<String, HttpError>> + Send>> {
        let url = url.to_owned();
        Box::pin(async move { do_reqwest_get(&url).await })
    }
}

async fn do_reqwest_get(url: &Url) -> Result<String, HttpError> {
    // TODO: converting to/from URL unnecessarily
    Ok(reqwest::get(url.as_str())
        .await?
        .error_for_status()?
        .text()
        .await?)
}

#[derive(Clone, Debug, PartialEq)]
enum ProjectVersion {
    Release(String),
    Commit(String),
    CommitBranch(String, String),
}

impl Display for ProjectVersion {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ProjectVersion::Release(s) => f.write_str(s),
            ProjectVersion::Commit(s) => f.write_str(s),
            ProjectVersion::CommitBranch(c, b) => {
                f.write_fmt(format_args!("{c} ({b})"))
            }
        }
    }
}

impl From<&str> for ProjectVersion {
    fn from(value: &str) -> Self {
        if value.contains('.') {
            ProjectVersion::Release(value.to_owned())
        } else {
            ProjectVersion::Commit(value.to_owned())
        }
    }
}

impl From<String> for ProjectVersion {
    fn from(value: String) -> Self {
        if value.contains('.') {
            ProjectVersion::Release(value)
        } else {
            ProjectVersion::Commit(value)
        }
    }
}

struct ProjectInfo {
    name: String,
    version: ProjectVersion,
}

#[derive(Error, Debug)]
enum CreateProjectInfoError {
    #[error("missing project name")]
    MissingName,
    #[error("missing project version")]
    MissingVersion,
}

impl ProjectInfo {
    fn from_args(args: &[String]) -> Result<Self, CreateProjectInfoError> {
        let name = args
            .first()
            .ok_or(CreateProjectInfoError::MissingName)?
            .clone();

        let version = args
            .get(1)
            .ok_or(CreateProjectInfoError::MissingVersion)?
            .as_str()
            .into();

        Ok(Self { name, version })
    }
}

fn substitute_release(
    url_template: &str,
    project: &str,
    release: &str,
) -> Result<Url, ParseError> {
    Url::parse(
        &url_template
            .replace("{project}", project)
            .replace("{release}", release),
    )
}

fn substitute_commit(
    url_template: &str,
    project: &str,
    commit: &str,
) -> Result<Url, ParseError> {
    Url::parse(
        &url_template
            .replace("{project}", project)
            .replace("{commit}", commit),
    )
}

#[cfg(test)]
mod test {
    use std::collections::HashMap;

    use super::*;

    struct FakeHttpClient {
        responses: HashMap<String, String>,
    }

    impl FakeHttpClient {
        fn new(responses: &[(&str, &str)]) -> Self {
            Self {
                responses: HashMap::from_iter(
                    responses.into_iter().map(|(url, res)| {
                        ((*url).to_owned(), (*res).to_owned())
                    }),
                ),
            }
        }
    }

    impl HttpClient for FakeHttpClient {
        fn get(
            &self,
            url: &Url,
        ) -> Pin<Box<dyn Future<Output = Result<String, HttpError>> + Send>>
        {
            let res = self
                .responses
                .get(url.as_str())
                .ok_or(HttpError::NotFound)
                .cloned();

            Box::pin(async { res })
        }
    }

    #[tokio::test]
    async fn can_get_data() {
        let http_client = FakeHttpClient::new(&[(
            "http://example.com/v12.1.0/Cargo.lock",
            r#"
[[package]]
name = "matrix-sdk-common"
version = "0.8.0"
source = "git+https://github.com/matrix-org/matrix-rust-sdk?branch=release-for-crypto-wasm-12#37c17cf854a70fe6f719c7fde49a6b0e4402988f"

[[package]]
name = "matrix-sdk-crypto"
version = "0.8.0"
source = "git+https://github.com/matrix-org/matrix-rust-sdk#37c17cf854a70fe6f719c7fde49a6b0e4402988f"
                "#,
        )]);

        let packages = [(
            "matrix-rust-sdk-crypto-wasm".to_owned(),
            PackageUrls::Rust {
                release_lock: "http://example.com/{release}/Cargo.lock"
                    .to_owned(),
                commit_lock: "http://example.com/{commit}/Cargo.lock"
                    .to_owned(),
                release_toml: None,
                commit_toml: None,
            },
        )]
        .into();

        let mut plugin = VersionsPlugin {
            http_client,
            packages,
        };

        let res = plugin
            .message(&IncomingMessage {
                text: "!versions matrix-rust-sdk-crypto-wasm v12.1.0"
                    .to_owned(),
                room_address: RoomAddress::CommandLine,
                from: Box::new("cmdline"),
            })
            .await
            .unwrap()
            .unwrap();

        assert_eq!(
            res.text,
            "\
                matrix-rust-sdk-crypto-wasm v12.1.0\n\
                ... matrix-sdk-common 37c17cf8 (release-for-crypto-wasm-12)\n\
                ... matrix-sdk-crypto 37c17cf8\n\
            "
        );
    }

    #[tokio::test]
    async fn refuses_to_process_unknown_package() {
        let http_client = FakeHttpClient::new(&[]);
        let packages = [].into();
        let mut plugin = VersionsPlugin {
            http_client,
            packages,
        };

        let res = plugin
            .message(&IncomingMessage {
                text: "!versions some-other-package v12.1.0".to_owned(),
                room_address: RoomAddress::CommandLine,
                from: Box::new("cmdline"),
            })
            .await
            .unwrap_err();

        assert_eq!(
            res.to_string(),
            "I don't know anything about package 'some-other-package'"
        );
    }

    #[test]
    fn formatting_single_dependency_is_one_line() {
        let deps = DepTree::with_release("proj", "1.0");
        assert_eq!(deps.to_string(), "proj 1.0\n");
    }

    #[test]
    fn formatting_a_single_chain_of_dependencies_draws_a_cascade() {
        let subsubproj = DepTree::with_commit("subsubproj", "46435b0");
        let mut subproj = DepTree::with_release("subproj", "v3");
        subproj.deps.push(subsubproj);
        let mut deps = DepTree::with_release("proj", "1.0");
        deps.deps.push(subproj);
        assert_eq!(
            deps.to_string(),
            "\
                proj 1.0\n\
                ... subproj v3\n\
                ... ... subsubproj 46435b0\n\
            "
        );
    }

    #[test]
    fn formatting_a_tree_of_dependencies_draws_a_tree() {
        let subsubproj11 = DepTree::with_commit("subsubproj11", "46435b0");
        let subsubproj12 = DepTree::with_commit("subsubproj12", "95465c4");
        let subsubproj21 = DepTree::with_commit("subsubproj21", "46435b0");
        let subsubproj22 = DepTree::with_commit("subsubproj22", "95465c4");
        let mut subproj1 = DepTree::with_release("subproj1", "v3");
        let mut subproj2 = DepTree::with_release("subproj2", "v3");
        subproj1.deps.push(subsubproj11);
        subproj1.deps.push(subsubproj12);
        subproj2.deps.push(subsubproj21);
        subproj2.deps.push(subsubproj22);
        let mut deps = DepTree::with_release("proj", "1.0");
        deps.deps.push(subproj1);
        deps.deps.push(subproj2);
        assert_eq!(
            deps.to_string(),
            "\
                proj 1.0\n\
                ... subproj1 v3\n\
                ... ... subsubproj11 46435b0\n\
                ... ... subsubproj12 95465c4\n\
                ... subproj2 v3\n\
                ... ... subsubproj21 46435b0\n\
                ... ... subsubproj22 95465c4\n\
            "
        );
    }

    #[test]
    fn parsing_a_cargo_lock_toml_finds_deps() {
        let mut parent = DepTree::with_release("parent", "1");
        parse_response(
            &["matrix-sdk-common"],
            &mut parent,
            r#"
[[package]]
name = "matrix-sdk-common"
version = "0.8.0"
source = "git+https://github.com/matrix-org/matrix-rust-sdk#37c17cf854a70fe6f719c7fde49a6b0e4402988f"
            "#,
        );

        assert_eq!(parent.deps.len(), 1);
        assert_eq!(
            parent.deps[0],
            DepTree::with_commit("matrix-sdk-common", "37c17cf8")
        );
    }

    #[test]
    fn boring_packages_are_ignored_from_cargo_lock() {
        let mut parent = DepTree::with_release("parent", "1");
        parse_response(
            &["matrix-sdk-common"],
            &mut parent,
            r#"
[[package]]
name = "some-other-package"
version = "0.8.0"
source = "git+https://github.com/matrix-org/matrix-rust-sdk?branch=release-for-crypto-wasm-12#37c17cf854a70fe6f719c7fde49a6b0e4402988f"

[[package]]
name = "matrix-sdk-common"
version = "0.8.0"
source = "git+https://github.com/matrix-org/matrix-rust-sdk?branch=release-for-crypto-wasm-12#37c17cf854a70fe6f719c7fde49a6b0e4402988f"
            "#,
        );

        assert_eq!(parent.deps.len(), 1);
        assert_eq!(
            parent.deps[0],
            DepTree::with_commit_branch(
                "matrix-sdk-common",
                "37c17cf8",
                "release-for-crypto-wasm-12"
            )
        );
    }

    #[test]
    fn can_extract_git_source_version() {
        let source = "git+https://github.com/matrix-org/\
            matrix-rust-sdk#37c17cf854a70fe6f719c7fde49a6b0e4402988f";

        assert_eq!(
            source_version(source),
            Some(ProjectVersion::Commit("37c17cf8".to_owned()))
        );
    }

    #[test]
    fn can_extract_git_source_version_and_branch() {
        let source = "git+https://github.com/matrix-org/\
            matrix-rust-sdk?branch=release-for-crypto-wasm-12\
            #37c17cf854a70fe6f719c7fde49a6b0e4402988f";

        assert_eq!(
            source_version(source),
            Some(ProjectVersion::CommitBranch(
                "37c17cf8".to_owned(),
                "release-for-crypto-wasm-12".to_owned()
            ))
        );
    }

    #[test]
    fn source_without_commit_id_gives_no_source_version() {
        let source = "registry+https://github.com/rust-lang/crates.io-index";
        assert_eq!(source_version(source), None);
    }
}
