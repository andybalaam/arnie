use clap::Parser;
use std::io;
use std::time::{SystemTime, UNIX_EPOCH};

use arnie::args::Args;
use arnie::run::run;

#[tokio::main]
async fn main() {
    let args = Args::parse();
    println!("Starting.");
    let random_seed = SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("System time is before the UNIX_EPOCH!")
        .as_secs();
    let res = run(
        args,
        tokio::io::stdin(),
        io::stdout(),
        io::stderr(),
        random_seed,
    )
    .await;
    match res {
        Ok(_) => println!("Stopping."),
        Err(e) => println!("Stopping with error: {}", e),
    }
}
