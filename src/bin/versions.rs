use arnie::{
    incoming_message::IncomingMessage,
    plugins::{plugin::Plugin, versions_plugin::VersionsPlugin},
    room_address::RoomAddress,
};
use itertools::Itertools;
use std::{env, iter};

#[tokio::main]
async fn main() {
    let mut versions_plugin =
        VersionsPlugin::new_real().expect("Failed to create VersionsPlugin");

    let response = versions_plugin
        .message(&IncomingMessage::new(
            iter::once("!versions".to_owned())
                .chain(env::args().skip(1))
                .join(" "),
            RoomAddress::CommandLine,
            "command_line",
        ))
        .await
        .expect("Failed to process supplied command")
        .expect("Response was empty");

    println!("{}", response.text);
}
