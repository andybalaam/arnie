use std::{iter::Peekable, str::Chars};

#[derive(Debug, PartialEq)]
pub(crate) struct HumanCommand {
    pub keywords: Vec<String>,
    pub args: Vec<String>,
}

#[derive(Debug, PartialEq)]
enum Token {
    LeadingWhitespace,
    Keyword(String),
    Argument(String),
}

impl HumanCommand {
    pub fn parse(input: &str) -> Option<Self> {
        let mut iter = input.chars().peekable();

        let first_keyword =
            if let Some(Token::Keyword(kw)) = next_token(&mut iter, true) {
                kw
            } else {
                return None;
            };

        let mut keywords = vec![first_keyword];
        let mut args = Vec::new();

        // Are we still looking for more leading keywords?
        let mut keyword_mode = true;

        while let Some(token) = next_token(&mut iter, keyword_mode) {
            match token {
                Token::LeadingWhitespace => {}
                Token::Keyword(keyword) => keywords.push(keyword),
                Token::Argument(new_content) => {
                    args.push(new_content);
                    // After we've seen one argument, everything else is
                    // treated as an argument, even if it looks like a command.
                    keyword_mode = false;
                }
            }
        }

        // Add the rest to content
        Some(HumanCommand { keywords, args })
    }

    pub fn content(&self) -> String {
        self.args.join(" ")
    }
}

/// Find the next token and consume any trailing whitespace
/// If keywords is true, treat any word starting with ! as a keyword. If not,
/// return it as a normal argument
fn next_token(iter: &mut Peekable<Chars>, keyword_mode: bool) -> Option<Token> {
    let word = next_word(iter);
    let word = match word {
        NextWord::EndOfStream => return None,
        NextWord::LeadingWhitespace => {
            return Some(Token::LeadingWhitespace);
        }
        NextWord::Word(word) => word,
    };

    if !keyword_mode {
        return Some(Token::Argument(word));
    }

    Some(match word.chars().next() {
        Some('!') => Token::Keyword(word),
        _ => Token::Argument(word),
    })
}

#[derive(Debug, PartialEq)]
enum NextWord {
    EndOfStream,
    Word(String),
    LeadingWhitespace,
}

/// Find the next word and consume any trailing whitespace
/// Returns and empty string if the next character is whitespace
/// Returns None if there are no more characters
fn next_word(iter: &mut Peekable<Chars>) -> NextWord {
    // If iterator is empty, return None immediately
    let first_char = iter.peek();
    let Some(first_char) = first_char else {
        return NextWord::EndOfStream;
    };
    let quoted = *first_char == '"';

    if quoted {
        // Skip leading "
        iter.next();
    }

    let keep_going = |c: &char| {
        if quoted {
            *c != '"'
        } else {
            !c.is_whitespace()
        }
    };

    // The next word is everything up to our end conditions
    let word: String = iter.take_while(keep_going).collect();

    let ret = if word.is_empty() && !quoted {
        NextWord::LeadingWhitespace
    } else {
        NextWord::Word(word)
    };

    // Consume any additional trailing whitespace
    while let Some(c) = iter.peek() {
        if !c.is_whitespace() {
            break;
        }
        iter.next();
    }

    ret
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn next_token_returns_none_for_empty_string() {
        let mut it = "".chars().peekable();
        assert_eq!(next_token(&mut it, true), None);
    }

    #[test]
    fn next_token_returns_arg_if_no_leading_exclamation_mark() {
        let mut it = "foo".chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("foo".to_owned()))
        );
    }

    #[test]
    fn next_token_treats_keyword_as_arg_if_not_in_keywords_mode() {
        let mut it = "!foo".chars().peekable();
        assert_eq!(
            next_token(&mut it, false),
            Some(Token::Argument("!foo".to_owned()))
        );
    }

    #[test]
    fn next_token_recognises_a_quoted_string_as_a_single_token() {
        let mut it = r#""foo bar" baz"#.chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("foo bar".to_owned()))
        );
    }

    #[test]
    fn next_word_recognises_leading_whitespace() {
        let mut it = r#"   baz"#.chars().peekable();
        assert_eq!(next_word(&mut it), NextWord::LeadingWhitespace);
    }

    #[test]
    fn next_word_returns_empty_quoted_string_as_an_empty_word() {
        let mut it = r#""" baz"#.chars().peekable();
        assert_eq!(next_word(&mut it), NextWord::Word("".to_owned()));
    }

    #[test]
    fn next_word_returns_unquoted_word() {
        let mut it = r#"foo baz"#.chars().peekable();
        assert_eq!(next_word(&mut it), NextWord::Word("foo".to_owned()));
    }

    #[test]
    fn next_word_returns_quoted_word() {
        let mut it = r#""foo" baz"#.chars().peekable();
        assert_eq!(next_word(&mut it), NextWord::Word("foo".to_owned()));
    }

    #[test]
    fn next_word_returns_word_at_end_of_stream() {
        let mut it = r#"baz"#.chars().peekable();
        assert_eq!(next_word(&mut it), NextWord::Word("baz".to_owned()));
    }

    #[test]
    fn next_word_recognises_end_of_stream() {
        let mut it = "".chars().peekable();
        assert_eq!(next_word(&mut it), NextWord::EndOfStream);
    }

    #[test]
    fn next_token_recognises_an_empty_quoted_string_as_a_single_token() {
        let mut it = r#""" baz"#.chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("".to_owned()))
        );
    }

    #[test]
    fn next_token_splits_quoted_tokens_after_quote() {
        let mut it = r#""foo bar"baz"#.chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("foo bar".to_owned()))
        );
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("baz".to_owned()))
        );
    }

    #[test]
    fn next_token_consumes_closing_quote() {
        let mut it = r#""foo bar" baz"#.chars().peekable();

        // Skip the first token
        next_token(&mut it, true);

        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("baz".to_owned()))
        );
    }

    #[test]
    fn next_token_returns_keyword_if_starts_with_exclamation_mark() {
        let mut it = "!foo".chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Keyword("!foo".to_owned()))
        );
    }

    #[test]
    fn next_token_returns_keyword_up_to_space() {
        let mut it = "!foo bar baz ".chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Keyword("!foo".to_owned()))
        );

        // And we consumed the keyword and trailing space
        assert_eq!(it.collect::<String>(), "bar baz ");
    }

    #[test]
    fn next_token_returns_keyword_up_to_any_white_space() {
        let mut it = "!foo\t\n bar baz ".chars().peekable();
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Keyword("!foo".to_owned()))
        );

        // And we consumed the keyword and all trailing spaces
        assert_eq!(it.collect::<String>(), "bar baz ");
    }

    #[test]
    fn next_token_notices_leading_whitespace() {
        let mut it = " !foo bar baz ".chars().peekable();
        assert_eq!(next_token(&mut it, true), Some(Token::LeadingWhitespace));
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Keyword("!foo".to_owned()))
        );
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("bar".to_owned()))
        );
        assert_eq!(
            next_token(&mut it, true),
            Some(Token::Argument("baz".to_owned()))
        );
    }

    #[test]
    fn empty_command_is_empty() {
        assert_eq!(HumanCommand::parse(""), None);
    }

    #[test]
    fn when_no_keywords_everything_is_content() {
        assert_eq!(HumanCommand::parse("here Is some !content"), None);
    }

    #[test]
    fn single_keyword_only() {
        assert_eq!(
            HumanCommand::parse("!arnie"),
            Some(HumanCommand {
                keywords: vec!["!arnie".to_owned()],
                args: Vec::new(),
            })
        );
    }

    #[test]
    fn keyword_and_content() {
        assert_eq!(
            HumanCommand::parse("!arnie Get Ooot!"),
            Some(HumanCommand {
                keywords: vec!["!arnie".to_owned()],
                args: vec!["Get".to_owned(), "Ooot!".to_owned()],
            })
        );
    }

    #[test]
    fn keyword_and_content_separated_by_various_whitespace() {
        assert_eq!(
            HumanCommand::parse("!arnie \t\n Get Ooot!"),
            Some(HumanCommand {
                keywords: vec!["!arnie".to_owned()],
                args: vec!["Get".to_owned(), "Ooot!".to_owned()],
            })
        );
    }

    #[test]
    fn multiple_keywords_at_beginning() {
        assert_eq!(
            HumanCommand::parse("!arnie \t !add \n Get Ooot!"),
            Some(HumanCommand {
                keywords: vec!["!arnie".to_owned(), "!add".to_owned()],
                args: vec!["Get".to_owned(), "Ooot!".to_owned()],
            })
        );
    }

    #[test]
    fn leading_whitespace_means_we_dont_recognise_a_keyword() {
        assert_eq!(HumanCommand::parse(" !arnie \t !add \n Get Ooot!"), None);
    }

    #[test]
    fn whitespace_within_content_is_preserved() {
        assert_eq!(
            HumanCommand::parse("!arnie !add Get  Ooot! \n"),
            Some(HumanCommand {
                keywords: vec!["!arnie".to_owned(), "!add".to_owned()],
                args: vec!["Get".to_owned(), "Ooot!".to_owned()],
            })
        );
    }

    #[test]
    fn nonascii_characters_dont_break_parsing() {
        assert_eq!(
            HumanCommand::parse("!arni\u{1F4A9} \t\n Get Ooot!"),
            Some(HumanCommand {
                keywords: vec!["!arni\u{1F4A9}".to_owned()],
                args: vec!["Get".to_owned(), "Ooot!".to_owned()],
            })
        );
    }

    #[test]
    fn keywords_after_non_keywords_are_treated_as_normal_arguments() {
        assert_eq!(
            HumanCommand::parse("!arnie !add This is A !NEW quote!"),
            Some(HumanCommand {
                keywords: sv(&["!arnie", "!add"]),
                args: sv(&["This", "is", "A", "!NEW", "quote!"])
            })
        );
    }

    #[test]
    fn double_quoted_strings_are_one_argument() {
        assert_eq!(
            HumanCommand::parse(r#"!arnie !add "Quote 1" "Quote 2""#),
            Some(HumanCommand {
                keywords: sv(&["!arnie", "!add"]),
                args: sv(&["Quote 1", "Quote 2"])
            })
        );
    }

    #[test]
    fn empty_quoted_string_is_an_argument() {
        assert_eq!(
            HumanCommand::parse(r#"!arnie !add "" """#),
            Some(HumanCommand {
                keywords: sv(&["!arnie", "!add"]),
                args: sv(&["", ""])
            })
        );
    }

    fn sv(input: &[&str]) -> Vec<String> {
        input.into_iter().map(|&s| s.to_owned()).collect()
    }
}
