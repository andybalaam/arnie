use async_trait::async_trait;
use tokio::io::{AsyncBufReadExt, AsyncRead, BufReader};
use tokio::sync::mpsc;

use crate::incoming_message::IncomingMessage;
use crate::message_receiver::MessageReceiver;
use crate::room_address::RoomAddress;

pub struct CommandLineReceiver<In: AsyncRead + Send + Unpin> {
    stdin: BufReader<In>,
}

impl<In: AsyncRead + Send + Unpin> CommandLineReceiver<In> {
    pub fn new(stdin: In) -> Self {
        Self {
            stdin: BufReader::new(stdin),
        }
    }
}

#[async_trait]
impl<In: AsyncRead + Send + Unpin> MessageReceiver for CommandLineReceiver<In> {
    async fn receive_forever(self, output: mpsc::Sender<IncomingMessage>) {
        let mut lines = self.stdin.lines();
        loop {
            let line = lines.next_line().await;
            match line {
                Err(_e) => {
                    // TODO: print error
                    break;
                }
                Ok(None) => {
                    // TODO: print message
                    break;
                }
                Ok(Some(line)) => {
                    output.send(create_message(line)).await.expect(
                        "Unable to pass message to bot from command \
                            line receiver",
                    );
                }
            }
        }
    }
}

fn create_message(body: String) -> IncomingMessage {
    IncomingMessage::new(body, RoomAddress::CommandLine, "__command_line__")
}

#[cfg(test)]
mod test {
    use super::create_message;

    #[test]
    fn content_of_command_line_message_is_added() {
        let msg = create_message("Hello Jonas".to_owned());
        assert_eq!(msg.text, "Hello Jonas");
    }

    #[test]
    fn sender_of_command_line_messages_is_commandline() {
        let msg = create_message("hi".to_owned());
        assert!(msg.from.canonically_equals("__command_line__"));
        assert_eq!(msg.from.id(), "__command_line__");
    }
}
