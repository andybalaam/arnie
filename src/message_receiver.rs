use async_trait::async_trait;
use tokio::sync::mpsc;

use crate::incoming_message::IncomingMessage;

/**
 * Something that can receive messages and call a callback for
 * each one it receives.
 */
#[async_trait]
pub trait MessageReceiver {
    /**
     * Receive messages, sending them to output.  Consumes this receiver.
     *
     * This method should only return when an error means that this receiver
     * is no longer usable.
     */
    async fn receive_forever(self, output: mpsc::Sender<IncomingMessage>);
}
