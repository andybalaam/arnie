use std::{
    future::Future,
    pin::Pin,
    sync::{Arc, Mutex},
};

use async_trait::async_trait;
use uuid::Uuid;

use crate::scheduler::Scheduler;

/// Implementation of [`Scheduler`] that really schedules tasks using
/// [`tokio_cron_scheduler`].
pub struct RealScheduler {
    scheduler: tokio_cron_scheduler::JobScheduler,
    uuids: Arc<Mutex<Vec<Uuid>>>,
}

impl RealScheduler {
    pub async fn new() -> Self {
        Self {
            scheduler: tokio_cron_scheduler::JobScheduler::new()
                .await
                .expect("Failed to initialise the tokio_cron_scheduler"),
            uuids: Arc::new(Mutex::new(Vec::new())),
        }
    }
}

#[async_trait]
impl Scheduler for RealScheduler {
    async fn add(
        &self,
        schedule: &str,
        mut f: Box<
            dyn FnMut() -> Pin<Box<dyn Future<Output = ()> + Send>>
                + Send
                + Sync,
        >,
    ) -> Result<(), ()> {
        let job = tokio_cron_scheduler::Job::new_cron_job_async(
            schedule,
            move |_uuid, _sch| f(),
        )
        .map_err(|_| ())?;

        self.uuids.lock().unwrap().push(job.guid());
        self.scheduler.add(job).await.expect("Failed to schedule");

        Ok(())
    }

    async fn remove(&self, index: usize) {
        let uuid = self.uuids.lock().unwrap().remove(index);
        self.scheduler
            .remove(&uuid)
            .await
            .expect("Failed to unschedule");
    }

    fn start(&self) -> anyhow::Result<()> {
        let real_scheduler_clone = self.scheduler.clone();
        tokio::spawn(async move { real_scheduler_clone.start().await });
        Ok(())
    }
}
