use std::collections::HashMap;

use async_trait::async_trait;
use redis::aio::ConnectionManager;
use redis::{AsyncCommands, Client, RedisResult};

use crate::database::Database;

pub struct RedisDatabase {
    connection: ConnectionManager,
}

impl RedisDatabase {
    pub async fn new(redis_url: String) -> RedisResult<RedisDatabase> {
        let client = Client::open(redis_url)?;
        let connection = ConnectionManager::new(client).await?;

        Ok(RedisDatabase { connection })
    }
}

#[async_trait]
impl Database for RedisDatabase {
    async fn get(&mut self, key: &str) -> RedisResult<Option<String>> {
        self.connection.get(key).await
    }

    async fn get_usize(&mut self, key: &str) -> RedisResult<Option<usize>> {
        self.connection.get(key).await
    }

    async fn exists(&mut self, key: &str) -> RedisResult<bool> {
        self.connection.exists(key).await
    }

    async fn set(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.connection.set(key, value).await
    }

    async fn incr(&mut self, key: &str, delta: i32) -> RedisResult<i32> {
        self.connection.incr(key, delta).await
    }

    async fn set_usize(&mut self, key: &str, value: usize) -> RedisResult<()> {
        self.connection.set(key, value).await
    }

    async fn lpush(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.connection.lpush(key, value).await
    }

    async fn lrem(
        &mut self,
        key: &str,
        count: isize,
        value: &str,
    ) -> RedisResult<()> {
        self.connection.lrem(key, count, value).await
    }

    async fn lset(
        &mut self,
        key: &str,
        index: isize,
        value: &str,
    ) -> RedisResult<()> {
        self.connection.lset(key, index, value).await
    }

    async fn llen(&mut self, key: &str) -> RedisResult<usize> {
        self.connection.llen(key).await
    }

    async fn lindex(
        &mut self,
        key: &str,
        index: isize,
    ) -> RedisResult<Option<String>> {
        self.connection.lindex(key, index).await
    }

    async fn lrange(
        &mut self,
        key: &str,
        start: isize,
        stop: isize,
    ) -> RedisResult<Vec<String>> {
        self.connection.lrange(key, start, stop).await
    }

    async fn hlen(&mut self, key: &str) -> RedisResult<usize> {
        self.connection.hlen(key).await
    }

    async fn hset(
        &mut self,
        key: &str,
        field: &str,
        value: &str,
    ) -> RedisResult<()> {
        self.connection.hset(key, field, value).await
    }

    async fn hget(
        &mut self,
        key: &str,
        field: &str,
    ) -> RedisResult<Option<String>> {
        self.connection.hget(key, field).await
    }

    async fn hgetall(
        &mut self,
        key: &str,
    ) -> RedisResult<HashMap<String, String>> {
        self.connection.hgetall(key).await
    }

    async fn hdel(&mut self, key: &str, field: &str) -> RedisResult<()> {
        self.connection.hdel(key, field).await
    }

    async fn del(&mut self, key: &str) -> RedisResult<()> {
        self.connection.del(key).await
    }
}
