use std::collections::HashMap;

use async_trait::async_trait;
use redis::RedisResult;

#[async_trait]
pub trait Database {
    async fn get(&mut self, key: &str) -> RedisResult<Option<String>>;
    async fn get_usize(&mut self, key: &str) -> RedisResult<Option<usize>>;
    async fn exists(&mut self, key: &str) -> RedisResult<bool>;
    async fn set(&mut self, key: &str, value: &str) -> RedisResult<()>;
    async fn set_usize(&mut self, key: &str, value: usize) -> RedisResult<()>;
    async fn incr(&mut self, key: &str, delta: i32) -> RedisResult<i32>;
    async fn lpush(&mut self, key: &str, value: &str) -> RedisResult<()>;
    /// Delete count elements from the list at key, or all if count == 0.
    /// If count < 0, delete backwards from the end of list.
    async fn lrem(
        &mut self,
        key: &str,
        count: isize,
        value: &str,
    ) -> RedisResult<()>;
    async fn lset(
        &mut self,
        key: &str,
        index: isize,
        value: &str,
    ) -> RedisResult<()>;
    async fn llen(&mut self, key: &str) -> RedisResult<usize>;
    async fn lindex(
        &mut self,
        key: &str,
        index: isize,
    ) -> RedisResult<Option<String>>;
    async fn lrange(
        &mut self,
        key: &str,
        start: isize,
        stop: isize,
    ) -> RedisResult<Vec<String>>;
    async fn hlen(&mut self, key: &str) -> RedisResult<usize>;
    async fn hset(
        &mut self,
        key: &str,
        field: &str,
        value: &str,
    ) -> RedisResult<()>;
    async fn hget(
        &mut self,
        key: &str,
        field: &str,
    ) -> RedisResult<Option<String>>;
    async fn hgetall(
        &mut self,
        key: &str,
    ) -> RedisResult<HashMap<String, String>>;
    async fn hdel(&mut self, key: &str, field: &str) -> RedisResult<()>;
    async fn del(&mut self, key: &str) -> RedisResult<()>;
}
