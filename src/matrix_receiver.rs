use std::time::Duration;

use async_trait::async_trait;
use matrix_sdk::config::SyncSettings;
use matrix_sdk::room::Room;
use matrix_sdk::ruma::api::client::receipt::create_receipt;
use matrix_sdk::ruma::events::receipt::ReceiptThread;
use matrix_sdk::ruma::events::room::member::{
    MembershipState, StrippedRoomMemberEvent,
};
use matrix_sdk::ruma::events::room::message::{
    MessageType, OriginalSyncRoomMessageEvent,
};
use matrix_sdk::Client;
use tokio::sync::mpsc;
use tokio::time::{sleep, Instant};

use crate::incoming_message::IncomingMessage;
use crate::message_receiver::MessageReceiver;
use crate::room_address::RoomAddress;

/// We refuse to sync again until at least this long since the last time
/// we synced.
const MIN_TIME_BETWEEN_SYNCS: Duration = Duration::from_secs(30);

pub struct MatrixReceiver {
    client: Client,
    matrix_admins: Vec<String>,
}

impl MatrixReceiver {
    pub async fn new(client: Client, matrix_admins: Vec<String>) -> Self {
        Self {
            client,
            matrix_admins,
        }
    }
}

#[async_trait]
impl MessageReceiver for MatrixReceiver {
    async fn receive_forever(self, output: mpsc::Sender<IncomingMessage>) {
        // We only want the bot to see messages that come after it starts, so
        // do a sync here to bypass any messages that have already been sent.
        // TODO: unwraps
        // TODO: this function is too long!
        self.client
            .sync_once(SyncSettings::default())
            .await
            .unwrap();

        let matrix_admins = self.matrix_admins.clone();
        self.client.add_event_handler(
            move |room_member: StrippedRoomMemberEvent,
                  client: Client,
                  room: Room| {
                on_member_event_join_room(
                    matrix_admins.clone(),
                    room_member,
                    client,
                    room,
                )
            },
        );

        self.client.add_event_handler(
            move |event: OriginalSyncRoomMessageEvent, room: Room| {
                let event_id = event.event_id.to_owned();
                let sender = event.sender;
                let output = output.clone();
                async move {
                    if let MessageType::Text(text_content) =
                        event.content.msgtype
                    {
                        let msg = IncomingMessage::new(
                            text_content.body,
                            RoomAddress::Matrix(room.room_id().to_string()),
                            sender,
                        );

                        // Pass the message we received back to the bot
                        // TODO: unwraps
                        output
                            .send(msg)
                            .await
                            .expect("Unable to pass message on to bot");

                        // Send a read receipt

                        // TODO: handle threads
                        let thread_type = ReceiptThread::Unthreaded;

                        room.send_single_receipt(
                            create_receipt::v3::ReceiptType::Read,
                            thread_type,
                            event_id,
                        )
                        .await
                        .unwrap_or_else(
                            // TODO: surface error
                            |e| println!("Unable to send read receipt! {}", e),
                        );
                    }
                }
            },
        );

        loop {
            let sync_start = Instant::now();
            let sync_result = self.client.sync(SyncSettings::default()).await;
            if let Err(e) = sync_result {
                // TODO: surface error
                println!("Sync returned with error: {e}");

                // Sleep until a reasonable time since the last sync started
                let since_last_sync = sync_start.elapsed();
                if since_last_sync < MIN_TIME_BETWEEN_SYNCS {
                    let sleep_time = MIN_TIME_BETWEEN_SYNCS - since_last_sync;
                    // TODO: surface message
                    println!(
                        "Waiting for a {}s gap between syncs.",
                        MIN_TIME_BETWEEN_SYNCS.as_secs()
                    );
                    sleep(sleep_time).await;
                }
            }
            // sync will never return Ok, so no else.
        }
    }
}

/**
 * Monitor the room for member events and join rooms we are invited to.
 */
async fn on_member_event_join_room(
    matrix_admins: Vec<String>,
    room_member: StrippedRoomMemberEvent,
    client: Client,
    room: Room,
) {
    // If this event is not about us, nothing to do.
    if room_member.state_key != client.user_id().unwrap() {
        return;
    }

    // If this is not an invitation, ignore it
    if room_member.content.membership != MembershipState::Invite {
        return;
    }

    let sender = room_member.sender;
    if !matrix_admins.contains(&sender.as_str().to_owned()) {
        println!(
            "Refusing to join room {} because the sender was {}",
            room.room_id(),
            sender
        );
        return;
    }

    // TODO: do some proper logging throughout this function
    println!("Autojoining room {}", room.room_id());
    let mut delay = 2;

    while let Err(err) = room.join().await {
        // retry autojoin due to synapse sending invites, before the
        // invited user can join for more information see
        // https://github.com/matrix-org/synapse/issues/4345
        eprintln!(
            "Failed to join room {} ({err:?}), retrying in {delay}s",
            room.room_id()
        );

        sleep(Duration::from_secs(delay)).await;
        delay *= 2;

        if delay > 3600 {
            eprintln!("Can't join room {} ({err:?})", room.room_id());
            break;
        }
    }
    println!("Successfully joined room {}", room.room_id());
}
