use crate::room_address::RoomAddress;

#[derive(Debug)]
pub struct IncomingMessage {
    pub text: String,
    pub room_address: RoomAddress,
    pub from: Box<dyn SenderAddress>,
}

impl IncomingMessage {
    pub fn new(
        text: String,
        room_address: RoomAddress,
        from: impl SenderAddress + 'static,
    ) -> Self {
        Self {
            text,
            room_address,
            from: Box::from(from),
        }
    }
}

pub trait SenderAddress: core::fmt::Debug + Send + Sync {
    /// Returns a canonical representation of a raw sender.
    fn canonicalize<'a>(&self, raw: &'a str) -> &'a str;
    /// Returns `true` if this sender should be considered equal to the
    /// provided `&str`.
    fn canonically_equals(&self, other: &str) -> bool;
    /// The an id for this message sender.
    /// Note: if two senders have the same ID they should be considered equal.
    fn id(&self) -> &str;
}

impl SenderAddress for String {
    fn canonicalize<'a>(&self, raw: &'a str) -> &'a str {
        raw
    }
    fn id(&self) -> &str {
        self
    }
    fn canonically_equals(&self, other: &str) -> bool {
        self == other
    }
}

impl SenderAddress for &str {
    fn canonicalize<'a>(&self, raw: &'a str) -> &'a str {
        raw
    }
    fn id(&self) -> &str {
        self
    }
    fn canonically_equals(&self, other: &str) -> bool {
        *self == other
    }
}
