use anyhow::Context;
use std::io::Write;
use std::sync::Arc;
use tokio::io::AsyncRead;
use tokio::sync::Mutex;

use crate::args::Args;
use crate::bot::Bot;
use crate::command_line_receiver::CommandLineReceiver;
use crate::command_line_sender::CommandLineSender;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::error_stream::ErrorStream;
use crate::matrix_config::MatrixConfig;
use crate::matrix_receiver::MatrixReceiver;
use crate::matrix_sender::MatrixSender;
use crate::memory_database::MemoryDatabase;
use crate::redis_database::RedisDatabase;
use crate::redis_store_info::RedisStoreInfo;

pub async fn run<
    In: 'static + AsyncRead + Send + Unpin,
    Ou: 'static + Write + Send,
    Er: 'static + Write,
>(
    args: Args,
    stdin: In,
    stdout: Ou,
    stderr: Er,
    random_seed: u64,
) -> anyhow::Result<i32> {
    let redis_url = args.redis_url.clone();
    if let Some(redis_url) = redis_url {
        let redis_db =
            Arc::new(Mutex::new(RedisDatabase::new(redis_url.clone()).await?));
        launch(
            args,
            stdin,
            stdout,
            stderr,
            redis_db,
            random_seed,
            Some(redis_url),
        )
        .await
    } else {
        let mem_db = Arc::new(Mutex::new(MemoryDatabase::new()));
        launch(args, stdin, stdout, stderr, mem_db, random_seed, None).await
    }
}

async fn launch<
    Db: 'static + Database + Send,
    In: 'static + AsyncRead + Send + Unpin,
    Ou: 'static + Write + Send,
    Er: 'static + Write,
>(
    args: Args,
    stdin: In,
    stdout: Ou,
    stderr: Er,
    db: Arc<Mutex<Db>>,
    random_seed: u64,
    redis_url: Option<String>,
) -> anyhow::Result<i32> {
    let errors = ErrorStream::new(stderr);
    let wrapped_db = DatabaseWrapper::new(db, args.db_prefix.clone());
    let mut matrix_db = wrapped_db.namespaced("matrix_sender_receiver|");
    let mut bot = Bot::new(wrapped_db, errors, random_seed, &args).await?;

    let store = make_store_info(redis_url, &args.db_prefix);
    bot.add_message_sender(Box::new(
        CommandLineSender::new(stdout)
            .context("Failed to create command line sender")?,
    ));
    bot.add_message_receiver(CommandLineReceiver::new(stdin));

    if args.enable_matrix {
        // TODO: unwraps
        let config = MatrixConfig::from_args(&args).await?;
        let client = config
            .create_client(&mut matrix_db, store)
            .await
            .context("Failed to make Matrix Client")?;

        bot.add_message_sender(Box::new(
            MatrixSender::new(client.clone()).await,
        ));
        bot.add_message_receiver(
            MatrixReceiver::new(client, args.matrix_admin_users).await,
        );
    }

    match bot.run().await {
        Ok(()) => Ok(0),
        Err(code) => Ok(code),
    }
}

fn make_store_info(
    redis_url: Option<String>,
    db_prefix: &str,
) -> Option<RedisStoreInfo> {
    if let Some(redis_url) = redis_url {
        Some(RedisStoreInfo {
            redis_url: redis_url.clone(),
            store_prefix: format!(
                "{}matrix_sender_receiver|crypto_store|",
                db_prefix
            ),
        })
    } else {
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::write_buffer::WriteBuffer;
    use clap::Parser;

    #[tokio::test]
    async fn can_start_and_stop_bot() {
        let args = Args::parse_from(["arnie", "--enable-matrix", "false"]);
        let stdin = "exit\n".as_bytes();
        let stdout = WriteBuffer::new();
        let stderr = WriteBuffer::new();
        run(args, stdin, stdout.clone(), stderr.clone(), 2)
            .await
            .expect("Should succeed.");

        assert_eq!(stdout.to_string(), ">>> Exiting\n>>> ");
        assert_eq!(stderr.to_string(), "");
    }
}
