use async_trait::async_trait;
use matrix_sdk::ruma::events::room::message::RoomMessageEventContent;
use matrix_sdk::ruma::{OwnedRoomId, RoomId};
use matrix_sdk::Client;
use maud::html;

use crate::message_sender::{MessageSender, SendError, WrongRoomAddressType};
use crate::outgoing_message::OutgoingMessage;
use crate::room_address::RoomAddress;

pub struct MatrixSender {
    client: Client,
}

impl MatrixSender {
    pub async fn new(client: Client) -> Self {
        Self { client }
    }
}

fn find_room_id(message: &OutgoingMessage) -> Result<OwnedRoomId, SendError> {
    if let RoomAddress::Matrix(rid) = &message.room_address {
        RoomId::parse(rid)
            .map_err(|_| SendError::new(Box::new(WrongRoomAddressType {})))
    } else {
        Err(SendError::new(Box::new(WrongRoomAddressType {})))
    }
}

#[async_trait]
impl MessageSender for MatrixSender {
    // TODO: unwrap
    async fn send(
        &mut self,
        message: &OutgoingMessage,
    ) -> Result<(), SendError> {
        let rid = find_room_id(message)?;
        if let Some(room) = self.client.get_room(&rid) {
            let text_body = format!("**{}**", message.text);
            let html_body = html! { strong { (message.text) } };
            let content =
                RoomMessageEventContent::text_html(&text_body, html_body);
            room.send(content).await.expect("Failed to send!");
        } else {
            eprintln!(
                "Attempted to send a message to a room we are not in: {}",
                rid
            );
        }

        Ok(())
    }

    fn is_addressed_by(&self, room_address: &RoomAddress) -> bool {
        matches!(room_address, RoomAddress::Matrix(_))
    }

    fn matrix_access_token(&self) -> Option<String> {
        self.client.access_token()
    }
}
