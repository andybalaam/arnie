use redis::RedisResult;
use std::{collections::HashMap, sync::Arc};
use tokio::sync::Mutex;

use crate::database::Database;

pub struct DatabaseWrapper<Db: Database> {
    db: Arc<Mutex<Db>>,
    prefix: String,
}

impl<Db: Database> Clone for DatabaseWrapper<Db> {
    fn clone(&self) -> Self {
        Self {
            db: Arc::clone(&self.db),
            prefix: self.prefix.clone(),
        }
    }
}

impl<Db: Database> DatabaseWrapper<Db> {
    pub fn new(db: Arc<Mutex<Db>>, prefix: String) -> Self {
        Self { db, prefix }
    }

    pub fn namespaced(&self, extra_prefix: &str) -> Self {
        let p = self.prefix.clone() + extra_prefix;
        Self::new(self.db.clone(), p)
    }
}

impl<Db: Database> DatabaseWrapper<Db> {
    pub async fn get(&mut self, key: &str) -> RedisResult<Option<String>> {
        self.db.lock().await.get(&self.key(key)).await
    }

    pub async fn get_usize(&mut self, key: &str) -> RedisResult<Option<usize>> {
        self.db.lock().await.get_usize(&self.key(key)).await
    }

    pub async fn incr(&mut self, key: &str, delta: i32) -> RedisResult<i32> {
        self.db.lock().await.incr(&self.key(key), delta).await
    }

    pub async fn exists(&mut self, key: &str) -> RedisResult<bool> where {
        self.db.lock().await.exists(&self.key(key)).await
    }

    pub async fn set(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.db.lock().await.set(&self.key(key), value).await
    }

    pub async fn set_usize(
        &mut self,
        key: &str,
        value: usize,
    ) -> RedisResult<()> {
        self.db.lock().await.set_usize(&self.key(key), value).await
    }

    pub async fn lpush(&mut self, key: &str, value: &str) -> RedisResult<()> {
        self.db.lock().await.lpush(&self.key(key), value).await
    }

    pub async fn lrem(
        &mut self,
        key: &str,
        count: isize,
        value: &str,
    ) -> RedisResult<()> {
        self.db
            .lock()
            .await
            .lrem(&self.key(key), count, value)
            .await
    }

    pub async fn lset(
        &mut self,
        key: &str,
        index: isize,
        value: &str,
    ) -> RedisResult<()> {
        self.db
            .lock()
            .await
            .lset(&self.key(key), index, value)
            .await
    }

    pub async fn llen(&self, key: &str) -> RedisResult<usize> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.llen(&k);
        f.await
    }

    pub async fn lindex(
        &self,
        key: &str,
        index: isize,
    ) -> RedisResult<Option<String>> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.lindex(&k, index);
        f.await
    }

    pub async fn lrange(
        &self,
        key: &str,
        start: isize,
        stop: isize,
    ) -> RedisResult<Vec<String>> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.lrange(&k, start, stop);
        f.await
    }

    pub async fn hlen(&self, key: &str) -> RedisResult<usize> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.hlen(&k);
        f.await
    }

    pub async fn hset(
        &self,
        key: &str,
        field: &str,
        value: &str,
    ) -> RedisResult<()> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.hset(&k, field, value);
        f.await
    }

    pub async fn hget(
        &self,
        key: &str,
        field: &str,
    ) -> RedisResult<Option<String>> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.hget(&k, field);
        f.await
    }

    pub async fn hgetall(
        &self,
        key: &str,
    ) -> RedisResult<HashMap<String, String>> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.hgetall(&k);
        f.await
    }

    pub async fn hdel(&mut self, key: &str, field: &str) -> RedisResult<()> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        let f = db.hdel(&k, field);
        f.await
    }

    pub async fn del(&self, key: &str) -> RedisResult<()> {
        let k = self.key(key);
        let mut db = self.db.lock().await;
        db.del(&k).await
    }

    fn key(&self, k: &str) -> String {
        self.prefix.clone() + k
    }
}

#[cfg(test)]
mod tests {
    // TODO tests
    #[test]
    fn x() {
        assert_eq!(2, 2);
    }
}
