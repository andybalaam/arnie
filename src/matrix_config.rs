use anyhow::anyhow;
use matrix_sdk::{
    matrix_auth::{MatrixSession, MatrixSessionTokens},
    ruma::OwnedUserId,
    Client, SessionMeta,
};

use crate::{
    args::Args, database::Database, database_wrapper::DatabaseWrapper,
    redis_store_info::RedisStoreInfo,
};

pub(crate) enum MatrixConfig {
    NewLogin(NewLoginMatrixConfig),
    ExistingLogin(ExistingLoginMatrixConfig),
}

pub(crate) struct NewLoginMatrixConfig {
    store_passphrase: Option<String>,
    homeserver_url: String,
    username: String,
    password: String,
    recovery_key: Option<String>,
}

pub(crate) struct ExistingLoginMatrixConfig {
    store_passphrase: Option<String>,
    access_token: String,
}

impl MatrixConfig {
    pub(crate) async fn from_args(args: &Args) -> anyhow::Result<Self> {
        if args.matrix_homeserver_url.is_some()
            || args.matrix_username.is_some()
            || args.matrix_password.is_some()
            || args.matrix_recovery_key.is_some()
        {
            if let (Some(homeserver_url), Some(username), Some(password)) = (
                &args.matrix_homeserver_url,
                &args.matrix_username,
                &args.matrix_password,
            ) {
                Ok(Self::NewLogin(NewLoginMatrixConfig {
                    store_passphrase: args.matrix_store_passphrase.clone(),
                    homeserver_url: homeserver_url.clone(),
                    username: username.clone(),
                    password: password.clone(),
                    recovery_key: args.matrix_recovery_key.clone(),
                }))
            } else {
                Err(
                    anyhow!(
                        "Missing argument: you must supply all of \
                        ARNIE_MATRIX_HOMESERVER_URL, ARNIE_MATRIX_USERNAME and ARNIE_MATRIX_PASSWORD, \
                        unless you already have an access token."
                    )
                )
            }
        } else if let Some(access_token) = &args.matrix_access_token {
            Ok(Self::ExistingLogin(ExistingLoginMatrixConfig {
                store_passphrase: args.matrix_store_passphrase.clone(),
                access_token: access_token.clone(),
            }))
        } else {
            Err(anyhow!(
                "Missing argument: you must supply login details \
                    or ARNIE_MATRIX_ACCESS_TOKEN."
            ))
        }
    }

    pub async fn create_client<Db: Database>(
        &self,
        db: &mut DatabaseWrapper<Db>,
        redis_store_info: Option<RedisStoreInfo>,
    ) -> anyhow::Result<Client> {
        // TODO: return an error when there is a missing entry in the DB.
        // TODO: later: start up fine even if missing entry and poll
        //       for good DB entries?

        println!("arnie: Connecting to matrix");

        match self {
            MatrixConfig::NewLogin(config) => {
                self.create_client_new(db, redis_store_info, config).await
            }
            MatrixConfig::ExistingLogin(config) => {
                self.create_client_existing(db, redis_store_info, config)
                    .await
            }
        }
    }

    async fn create_client_new<Db: Database>(
        &self,
        db: &mut DatabaseWrapper<Db>,
        redis_store_info: Option<RedisStoreInfo>,
        config: &NewLoginMatrixConfig,
    ) -> anyhow::Result<Client> {
        println!("arnie: Logging in as a new device");

        db.set("homeserver_url", &config.homeserver_url)
            .await
            .expect("Failed to store homeserver_url");

        let client_builder =
            Client::builder().homeserver_url(&config.homeserver_url);

        let client = if let Some(redis_store_info) = redis_store_info {
            println!("arnie: Storing Matrix info in Redis");

            let store_config = matrix_sdk_redis::make_store_config(
                &redis_store_info.redis_url,
                config.store_passphrase.as_deref(),
                &redis_store_info.store_prefix,
            )
            .await?;
            client_builder.store_config(store_config).build().await?
        } else {
            println!("arnie: No Redis info provided: I will forget everything when I stop!");

            // Otherwise, by default we use a memory store
            client_builder.build().await?
        };

        let login = client
            .matrix_auth()
            .login_username(&config.username, &config.password);

        let response = login
            .initial_device_display_name("arnie")
            .send()
            .await
            .unwrap();

        db.set("device_id", response.device_id.as_str())
            .await
            .expect("Error saving device_id");

        db.set("user_id", response.user_id.as_str())
            .await
            .expect("Error saving user_id");

        println!(
            "arnie: Connected to Matrix (device id={})",
            response.device_id.as_str()
        );

        if let Some(recovery_key) = &config.recovery_key {
            println!("arnie: Securing our device using the recovery key");
            client
                .encryption()
                .recovery()
                .recover(recovery_key)
                .await
                .expect("Failed to recover using recovery key");

            println!(
                "arnie: Successfully secured this device using the recovery key"
            );
        } else {
            println!(
                "arnie: No recovery key provided, so this device is insecure."
            );
        }

        Ok(client)
    }

    async fn create_client_existing<Db: Database>(
        &self,
        db: &mut DatabaseWrapper<Db>,
        redis_store_info: Option<RedisStoreInfo>,
        config: &ExistingLoginMatrixConfig,
    ) -> anyhow::Result<Client> {
        println!("arnie: Logging in to an existing device");

        let user_id: OwnedUserId = db
            .get("user_id")
            .await?
            .ok_or(anyhow!(
                "Missing information: user_id. Did you log in first?"
            ))?
            .try_into()
            .map_err(|_| anyhow!("Failed to convert user_id"))?;

        let homeserver_url = db.get("homeserver_url").await?.ok_or(anyhow!(
            "Missing information: homeserver_url. Did you log in first?"
        ))?;

        let device_id = db.get("device_id").await?.ok_or(anyhow!(
            "Missing information: device_id. Did you log in first?"
        ))?;

        let client_builder = Client::builder().homeserver_url(homeserver_url);

        let client = if let Some(redis_store_info) = redis_store_info {
            println!("arnie: Storing Matrix info in Redis");
            let store_config = matrix_sdk_redis::make_store_config(
                &redis_store_info.redis_url,
                config.store_passphrase.as_deref(),
                &redis_store_info.store_prefix,
            )
            .await?;
            client_builder.store_config(store_config).build().await?
        } else {
            println!("arnie: No Redis info provided: I will forget everything when I stop!");
            // Otherwise, by default we use a memory store
            client_builder.build().await?
        };

        let session = MatrixSession {
            meta: SessionMeta {
                user_id,
                device_id: device_id.into(),
            },
            tokens: MatrixSessionTokens {
                access_token: config.access_token.clone(),
                refresh_token: None,
            },
        };
        client.matrix_auth().restore_session(session).await?;

        Ok(client)
    }
}
