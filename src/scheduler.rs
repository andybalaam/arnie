use std::{future::Future, pin::Pin};

use async_trait::async_trait;

/// Something that allows us to schedule and remove recurring tasks to be
/// completed at a specific time every day, week etc.
///
/// You probably want to instantiate a [`crate::real_scheduler::RealScheduler`].
///
/// This trait allows us to test code that depends on a scheduler without
/// instantiating a real one.
#[async_trait]
pub trait Scheduler: Send + Sync {
    /// Schedule a recurring task to happen.
    /// `schedule` is a crontab-line string as specified by croner:
    /// https://github.com/Hexagon/croner-rust?tab=readme-ov-file#pattern
    async fn add(
        &self,
        schedule: &str,
        mut f: Box<
            dyn FnMut() -> Pin<Box<dyn Future<Output = ()> + Send>>
                + Send
                + Sync,
        >,
    ) -> Result<(), ()>;

    /// Remove the task with the supplied index
    /// TODO: return and ID and allow removing by ID
    async fn remove(&self, index: usize);

    /// Start the scheduler. Not tasks are executed unless this has been
    /// called.
    fn start(&self) -> anyhow::Result<()>;
}
