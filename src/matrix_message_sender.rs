use crate::incoming_message::SenderAddress;
use matrix_sdk::ruma::OwnedUserId;
use matrix_sdk::ruma::UserId;

impl SenderAddress for OwnedUserId {
    fn canonicalize<'a>(&self, raw: &'a str) -> &'a str {
        match <&UserId>::try_from(raw) {
            Err(_) => raw,
            Ok(id) => id.localpart(),
        }
    }
    fn canonically_equals(&self, other: &str) -> bool {
        self.canonicalize(other) == self.localpart()
    }

    fn id(&self) -> &str {
        self.as_str()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn matrix_message_sender_is_equal_to_sender_id() {
        let matrix_sender =
            OwnedUserId::try_from("@zealous:example.com").unwrap();
        assert!(matrix_sender.canonically_equals("@zealous:example.com"));
    }
    #[test]
    fn matrix_message_sender_is_equal_to_sender_name() {
        let matrix_sender =
            OwnedUserId::try_from("@zealous:example.com").unwrap();
        assert!(matrix_sender.canonically_equals("zealous"));
    }
}
