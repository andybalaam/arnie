use std::fmt::{Display, Formatter};
use std::io;
use std::sync::{Arc, Mutex};

/**
 * A buffer that may be written to by multiple
 * owners.  Useful for testing code that expects
 * to own a Write instance.
 */
#[derive(Clone, Default)]
pub struct WriteBuffer {
    contents: Arc<Mutex<Vec<u8>>>,
}

impl WriteBuffer {
    pub fn new() -> WriteBuffer {
        WriteBuffer {
            contents: Arc::new(Mutex::new(Vec::new())),
        }
    }
}

impl Display for WriteBuffer {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str(&String::from_utf8_lossy(&self.contents.lock().unwrap()))
    }
}

impl io::Write for WriteBuffer {
    fn write(&mut self, buf: &[u8]) -> Result<usize, std::io::Error> {
        let ret = buf.len();
        self.contents.lock().unwrap().extend_from_slice(buf);
        Ok(ret)
    }

    fn flush(&mut self) -> Result<(), std::io::Error> {
        Ok(())
    }
}
