use std::sync::Arc;

use itertools::Itertools as _;
use tokio::sync::mpsc;

use crate::args::Args;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::error_stream::ErrorStream;
use crate::incoming_message::IncomingMessage;
use crate::message_receiver::MessageReceiver;
use crate::message_receivers_list::MessageReceiversList;
use crate::message_sender::MessageSender;
use crate::outgoing_message::OutgoingMessage;
use crate::plugins::admin_plugin::{AdminAction, AdminPlugin, AdminResponse};
use crate::plugins::arnie_plugin::ArniePlugin;
use crate::plugins::date_plugin::DatePlugin;
use crate::plugins::goldstar_plugin::GoldstarPlugin;
use crate::plugins::greet_plugin::GreetPlugin;
use crate::plugins::plugin::Plugin;
use crate::plugins::system_admin_plugin::SystemAdminPlugin;
use crate::plugins::todo_plugin::TodoPlugin;
use crate::real_scheduler::RealScheduler;
use crate::room_address::RoomAddress;
use crate::scheduler::Scheduler;

const ERROR_CODE_ADMIN_SEND_FAILED: i32 = 14;

// TODO: list all available commands

pub struct Bot<Db: Database + Send> {
    _db: DatabaseWrapper<Db>,
    message_receivers: MessageReceiversList,
    message_senders: Vec<Box<dyn MessageSender>>,
    admin_plugins: Vec<Box<dyn AdminPlugin>>,
    plugins: Vec<Box<dyn Plugin>>,
    errors: ErrorStream,
    messages_channel_receiver: mpsc::UnboundedReceiver<OutgoingMessage>,
    matrix_admin_users: Vec<String>,
}

impl<Db: 'static + Database + Send> Bot<Db> {
    pub async fn new(
        db: DatabaseWrapper<Db>,
        errors: ErrorStream,
        random_seed: u64,
        args: &Args,
    ) -> anyhow::Result<Bot<Db>> {
        let (messages_channel_sender, messages_channel_receiver) =
            mpsc::unbounded_channel();

        let scheduler: Arc<dyn Scheduler> =
            Arc::new(RealScheduler::new().await);
        scheduler.start()?;

        let system_admin_plugin =
            Box::new(SystemAdminPlugin::new(db.namespaced("system")).await);

        let arnie_plugin = Box::new(
            ArniePlugin::new(db.namespaced("arnie|"), random_seed).await?,
        );

        let goldstar_plugin =
            Box::new(GoldstarPlugin::new(db.namespaced("goldstar|")).await?);

        let greet_plugin = Box::new(
            GreetPlugin::new(
                db.namespaced("greet|"),
                messages_channel_sender.clone(),
                Arc::clone(&scheduler),
            )
            .await,
        );

        let todo_plugin = Box::new(TodoPlugin::new(db.namespaced("todo|")));

        let date_plugin = Box::new(DatePlugin::new_real());

        Ok(Bot {
            _db: db,
            message_receivers: MessageReceiversList::new(errors.clone()),
            message_senders: Vec::new(),
            admin_plugins: vec![system_admin_plugin],
            plugins: vec![
                arnie_plugin,
                goldstar_plugin,
                greet_plugin,
                todo_plugin,
                date_plugin,
            ],
            errors,
            messages_channel_receiver,
            matrix_admin_users: args.matrix_admin_users.clone(),
        })
    }

    pub fn add_message_sender<M>(&mut self, message_sender: Box<M>)
    where
        M: MessageSender + 'static,
    {
        self.message_senders.push(message_sender);
    }

    pub fn add_message_receiver<M>(&mut self, message_receiver: M)
    where
        M: MessageReceiver + 'static,
    {
        self.message_receivers.add(message_receiver);
    }

    /**
     * Run the bot until we decide to exit.
     *
     * Returns an error indicating the status code to exit with.
     */
    pub async fn run(&mut self) -> Result<(), i32> {
        loop {
            tokio::select! {
                to_send = self.messages_channel_receiver.recv() => {
                    if let Some(to_send) = to_send {
                        self.send_message(to_send).await;
                    };
                },
                message = self.message_receivers.next_message() => {
                    self.process(&message).await?;
                }
            };
        }
    }
    /**
     * Deal with a message.
     *
     * Returns an error only if the whole process should stop.
     */
    async fn process(&mut self, message: &IncomingMessage) -> Result<(), i32> {
        self.process_admin_plugins(message).await?;
        self.process_plugins(message).await;
        Ok(())
    }

    /**
     * Send messages to the admin plugins and deal with their responses.
     *
     * In particular, returns an error only if the whole process
     * should stop.
     */
    async fn process_admin_plugins(
        &mut self,
        message: &IncomingMessage,
    ) -> Result<(), i32> {
        // Only listen to people talking on the command line, or specific admin
        // matrix users
        if matches!(&message.room_address, RoomAddress::CommandLine)
            || (matches!(&message.room_address, RoomAddress::Matrix(..))
                && self
                    .matrix_admin_users
                    .iter()
                    .map(|s| s.as_str())
                    .contains(&message.from.id()))
        {
            let mut responses = Vec::new();
            for admin_plugin in self.admin_plugins.iter_mut() {
                responses.push(admin_plugin.message(message).await);
            }

            for res in responses {
                self.process_admin_response(message, res).await?;
            }
        }
        Ok(())
    }

    async fn process_admin_response(
        &mut self,
        message: &IncomingMessage,
        res: anyhow::Result<AdminResponse>,
    ) -> Result<(), i32> {
        match res {
            Ok(AdminResponse {
                action: AdminAction::Exit(code),
                message: response,
            }) => {
                self.handle_admin_response_message(message, response)
                    .await?;
                Err(code)
            }
            Ok(AdminResponse {
                action: AdminAction::Error(module, error_message),
                message: response,
            }) => {
                self.handle_admin_response_message(message, response)
                    .await?;
                self.errors.send(&module, &error_message);
                Ok(())
            }
            Ok(AdminResponse {
                action: AdminAction::MatrixAccessToken,
                message: _,
            }) => {
                let mut access_token = None;
                for sender in &self.message_senders {
                    if let Some(acc) = sender.matrix_access_token() {
                        access_token = Some(acc);
                        break;
                    }
                }
                if let Some(access_token) = access_token {
                    let response = Some(OutgoingMessage {
                        text: access_token,
                        room_address: message.room_address.clone(),
                    });
                    self.handle_admin_response_message(message, response)
                        .await?;
                }
                Ok(())
            }
            Ok(AdminResponse {
                action: AdminAction::None,
                message: response,
            }) => {
                self.handle_admin_response_message(message, response)
                    .await?;
                Ok(())
            }
            Err(e) => {
                // Log the error, but don't quit the program
                self.errors
                    .send("bot::process_admin_plugins", &e.to_string());
                Ok(())
            }
        }
    }

    async fn handle_admin_response_message(
        &mut self,
        original_message: &IncomingMessage,
        response: Option<OutgoingMessage>,
    ) -> Result<(), i32> {
        if let Some(response) = response {
            let mut errors = Vec::new();

            // Admin response goes back to the room the request came from
            let original_room_address = &original_message.room_address;
            for message_sender in self.message_senders.iter_mut() {
                if message_sender.is_addressed_by(&original_room_address) {
                    let r = OutgoingMessage {
                        text: response.text.clone(),
                        room_address: original_room_address.clone(),
                    };
                    let send_res = message_sender.send(&r).await;
                    if let Err(error) = send_res {
                        errors.push(error.to_string());
                    };
                }
            }

            if !errors.is_empty() {
                for error in errors {
                    self.errors.send(
                        "bot::process_admin_response",
                        &error.to_string(),
                    );
                }
                return Err(ERROR_CODE_ADMIN_SEND_FAILED);
            }
        }
        Ok(())
    }

    /**
     * Send messages to the plugins and deal with their responses.
     */
    async fn process_plugins(&mut self, message: &IncomingMessage) {
        for plugin in self.plugins.iter_mut() {
            let res = plugin.message(message).await;
            match res {
                Ok(Some(response)) => {
                    // TODO: figure out why I can't call self.send_message here
                    for message_sender in self
                        .message_senders
                        .iter_mut()
                        .filter(|message_sender| {
                            message_sender
                                .is_addressed_by(&response.room_address)
                        })
                    {
                        message_sender.send(&response).await.unwrap();
                    }
                }
                Ok(None) => {} // No message to send, just continue
                Err(e) => {
                    // TODO: surface an error
                    println!("Error from plugin: {e}");
                }
            }
        }
    }

    async fn send_message(&mut self, message: OutgoingMessage) {
        for message_sender in
            self.message_senders.iter_mut().filter(|message_sender| {
                message_sender.is_addressed_by(&message.room_address)
            })
        {
            message_sender.send(&message).await.unwrap();
        }
    }
}

#[cfg(test)]
mod test {
    use async_trait::async_trait;
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use crate::{
        command_line_sender::CommandLineSender,
        memory_database::MemoryDatabase, message_sender::SendError,
        room_address::RoomAddress, write_buffer::WriteBuffer,
    };

    use super::*;

    struct FakeMatrixSender {}

    #[async_trait]
    impl MessageSender for FakeMatrixSender {
        async fn send(
            &mut self,
            _message: &OutgoingMessage,
        ) -> Result<(), SendError> {
            todo!("No messages should be sent to the FakeMatrixSender in this test");
        }

        fn is_addressed_by(&self, room_address: &RoomAddress) -> bool {
            match room_address {
                RoomAddress::Matrix(_) => true,
                _ => false,
            }
        }

        fn matrix_access_token(&self) -> Option<String> {
            None
        }
    }

    struct EchoPlugin {}

    impl EchoPlugin {
        fn new() -> Self {
            Self {}
        }
    }

    #[async_trait]
    impl Plugin for EchoPlugin {
        async fn message(
            &mut self,
            message: &IncomingMessage,
        ) -> anyhow::Result<Option<OutgoingMessage>> {
            Ok(Some(message.into()))
        }
    }

    async fn new_bot(
        args: &Args,
    ) -> (Bot<MemoryDatabase>, WriteBuffer, WriteBuffer) {
        let stdout = WriteBuffer::new();
        let stderr = WriteBuffer::new();

        let mem_db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from(""),
        );
        let bot = Bot::new(mem_db, ErrorStream::new(stderr.clone()), 1, args)
            .await
            .expect("Failed to create bot");

        (bot, stdout, stderr)
    }

    #[tokio::test]
    async fn messages_go_to_the_right_sender() {
        // Given a bot that can send, and a plugin that echoes
        let (mut bot, stdout, _stderr) = new_bot(&Args::empty()).await;

        bot.add_message_sender(Box::new(
            CommandLineSender::new(stdout.clone()).unwrap(),
        ));
        bot.add_message_sender(Box::new(FakeMatrixSender {}));

        bot.plugins.push(Box::new(EchoPlugin::new()));

        // When a message comes in
        let incoming_message = IncomingMessage::new(
            "hello echo".to_owned(),
            RoomAddress::CommandLine,
            "goofy".to_owned(),
        );
        bot.process(&incoming_message).await.unwrap();

        // Then we sent out the echo
        assert_eq!(stdout.to_string(), ">>> hello echo\n>>> ");

        // We know the FakeMatrixSender wasn't sent it because it will
        // panic if it is sent a message.
    }

    struct FakeAdminPlugin {
        messages_received: Arc<Mutex<Vec<(String, String)>>>,
    }

    impl Default for FakeAdminPlugin {
        fn default() -> Self {
            Self {
                messages_received: Default::default(),
            }
        }
    }

    #[async_trait]
    impl AdminPlugin for FakeAdminPlugin {
        async fn message(
            &mut self,
            message: &IncomingMessage,
        ) -> anyhow::Result<AdminResponse> {
            self.messages_received
                .lock()
                .await
                .push((message.room_address.to_string(), message.text.clone()));

            Ok(AdminResponse {
                action: AdminAction::None,
                message: None,
            })
        }
    }

    #[tokio::test]
    async fn admin_commands_are_ignored_if_not_from_an_admin() {
        // Given a bot with no admins set up (except the default command line)
        let (mut bot, _stdout, _stderr) = new_bot(&Args::empty()).await;

        let admin_plugin = FakeAdminPlugin::default();
        let messages_received = Arc::clone(&admin_plugin.messages_received);
        bot.admin_plugins.push(Box::new(admin_plugin));

        // When we receive a message from Matrix
        let incoming_message = IncomingMessage::new(
            "hello echo".to_owned(),
            RoomAddress::Matrix("!45:s.co".to_owned()),
            "goofy".to_owned(),
        );
        bot.process(&incoming_message).await.unwrap();

        // Then the admin plugin doesn't see it
        assert!(messages_received.lock().await.is_empty());
    }

    #[tokio::test]
    async fn admin_commands_from_command_line_are_accepted() {
        // Given a bot with no admins set up (except the default command line)
        let (mut bot, _stdout, _stderr) = new_bot(&Args::empty()).await;

        let admin_plugin = FakeAdminPlugin::default();
        let messages_received = Arc::clone(&admin_plugin.messages_received);
        bot.admin_plugins.push(Box::new(admin_plugin));

        // When we receive a message from command line
        let incoming_message = IncomingMessage::new(
            "!print_error foo".to_owned(),
            RoomAddress::CommandLine,
            "goofy".to_owned(),
        );
        bot.process(&incoming_message).await.unwrap();

        // Then the admin plugin sees it, because command line is always an
        // admin
        assert_eq!(
            &*messages_received.lock().await,
            &[("CommandLine:".to_owned(), "!print_error foo".to_owned())]
        );
    }

    #[tokio::test]
    async fn admin_commands_from_admin_matrix_user_are_accepted() {
        // Given a bot with a matrix admin set up
        let mut args = Args::empty();
        args.matrix_admin_users
            .push("@goofy:example.com".to_owned());

        let (mut bot, _stdout, _stderr) = new_bot(&args).await;

        let admin_plugin = FakeAdminPlugin::default();
        let messages_received = Arc::clone(&admin_plugin.messages_received);
        bot.admin_plugins.push(Box::new(admin_plugin));

        // When we receive a message from the admin
        let incoming_message_from_goofy = IncomingMessage::new(
            "goofy says exit".to_owned(),
            RoomAddress::Matrix("!45:s.co".to_owned()),
            "@goofy:example.com".to_owned(),
        );
        bot.process(&incoming_message_from_goofy).await.unwrap();

        // And another from someone else
        let incoming_message_from_ethel = IncomingMessage::new(
            "ethel says exit".to_owned(),
            RoomAddress::Matrix("!45:s.co".to_owned()),
            "@ethel:example.com".to_owned(),
        );
        bot.process(&incoming_message_from_ethel).await.unwrap();

        // Then the admin plugin only sees the message from the admin
        assert_eq!(
            &*messages_received.lock().await,
            &[("Matrix:!45:s.co".to_owned(), "goofy says exit".to_owned())]
        );
    }
}
