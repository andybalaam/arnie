use std::fmt::Display;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum RoomAddress {
    Admin,
    CommandLine,
    Matrix(String), // room_id
}

impl Display for RoomAddress {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let (protocol, name) = match self {
            Self::Admin => ("Admin", ""),
            Self::CommandLine => ("CommandLine", ""),
            Self::Matrix(name) => ("Matrix", name.as_str()),
        };

        f.write_str(&format!("{}:{}", protocol, name))
    }
}

#[cfg(test)]
mod test {
    use crate::room_address::RoomAddress;

    #[test]
    fn can_turn_a_command_line_room_into_a_string() {
        assert_eq!(RoomAddress::CommandLine.to_string(), "CommandLine:");
    }

    #[test]
    fn can_turn_an_admin_room_into_a_string() {
        assert_eq!(RoomAddress::Admin.to_string(), "Admin:");
    }

    #[test]
    fn can_turn_a_matrix_room_into_a_string() {
        assert_eq!(
            RoomAddress::Matrix("#andybalaam:matrix.org".to_string())
                .to_string(),
            "Matrix:#andybalaam:matrix.org"
        );
    }
}
