use crate::{incoming_message::IncomingMessage, room_address::RoomAddress};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct OutgoingMessage {
    pub text: String,
    pub room_address: RoomAddress,
}

impl OutgoingMessage {
    pub fn new(text: String, room_address: RoomAddress) -> Self {
        Self { text, room_address }
    }
}

impl From<&IncomingMessage> for OutgoingMessage {
    fn from(incoming: &IncomingMessage) -> Self {
        Self {
            text: incoming.text.clone(),
            room_address: incoming.room_address.clone(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::{incoming_message::IncomingMessage, room_address::RoomAddress};

    use super::OutgoingMessage;

    #[test]
    fn can_make_outgoing_message_from_incoming() {
        let incoming = IncomingMessage::new(
            "hi msg Y".to_owned(),
            RoomAddress::CommandLine,
            "itsame".to_owned(),
        );
        let outgoing: OutgoingMessage = (&incoming).into();
        assert_eq!(outgoing.text, "hi msg Y");
        assert_eq!(outgoing.room_address, RoomAddress::CommandLine);
    }
}
