use std::io::Write;
use std::sync::{Arc, Mutex};

#[derive(Clone)]
pub struct ErrorStream {
    err: Arc<Mutex<dyn Write>>,
}

impl ErrorStream {
    pub fn new<Err: 'static + Write>(err: Err) -> Self {
        Self {
            err: Arc::new(Mutex::new(err)),
        }
    }

    pub fn send(&self, module: &str, message: &str) {
        let sent = self.do_send(module, message);
        if sent.is_err() {
            let msg = format!(
                "Failed to write to error stream!\n\
                Message:\n\
                {}: {}\n",
                module, message
            );
            std::io::stderr()
                .write_all(msg.as_bytes())
                .unwrap_or_else(|_| {
                    panic!("Failed to write to stderr.\n{}", msg)
                });
        }
    }

    fn do_send(&self, module: &str, message: &str) -> Result<(), ()> {
        let mut e = self.err.lock().map_err(|_| ())?;
        writeln!(e, "{}: {}", module, message).map_err(|_| ())?;
        Ok(())
    }
}
