use clap::{builder::ValueParser, ArgAction, Parser};

pub const VERSION: &str = env!("CARGO_PKG_VERSION");

#[derive(Debug, Parser)]
#[clap(
    version = env!("CARGO_PKG_VERSION"),
    author = env!("CARGO_PKG_AUTHORS"),
    name = env!("CARGO_PKG_NAME"),
    about = env!("CARGO_PKG_DESCRIPTION"),
)]
pub struct Args {
    #[clap(short, long, env = "ARNIE_REDIS_URL")]
    pub redis_url: Option<String>,

    #[clap(short, long, default_value = "", env = "ARNIE_DB_PREFIX")]
    pub db_prefix: String,

    #[clap(long, env = "ARNIE_MATRIX_HOMESERVER_URL")]
    pub matrix_homeserver_url: Option<String>,

    #[clap(long, env = "ARNIE_MATRIX_USERNAME")]
    pub matrix_username: Option<String>,

    #[clap(long, env = "ARNIE_MATRIX_PASSWORD")]
    pub matrix_password: Option<String>,

    #[clap(long, env = "ARNIE_MATRIX_STORE_PASSPHRASE")]
    pub matrix_store_passphrase: Option<String>,

    #[clap(long, env = "ARNIE_MATRIX_RECOVERY_KEY")]
    pub matrix_recovery_key: Option<String>,

    #[clap(long, env = "ARNIE_MATRIX_ACCESS_TOKEN")]
    pub matrix_access_token: Option<String>,

    #[clap(long, env = "ARNIE_MATRIX_ADMIN_USERS", value_delimiter = ',')]
    pub matrix_admin_users: Vec<String>,

    #[clap(
        long,
        action = ArgAction::Set,
        env = "ARNIE_ENABLE_MATRIX",
        value_parser = ValueParser::bool(),
        default_value_t = true
    )]
    pub enable_matrix: bool,
}

impl Args {
    /// For testing
    pub fn empty() -> Self {
        Self {
            redis_url: None,
            db_prefix: "".to_owned(),
            matrix_homeserver_url: None,
            matrix_username: None,
            matrix_password: None,
            matrix_store_passphrase: None,
            matrix_recovery_key: None,
            matrix_access_token: None,
            matrix_admin_users: Vec::new(),
            enable_matrix: true,
        }
    }
}
