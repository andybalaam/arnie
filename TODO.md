# TODO
+ Timed greeter plugin
+ Fix bug where Arnie always says Awight at the same time
+ Other personalities, not just arnie
+ !todo command
+ Allow verifying your device with a Recovery Key
+ Allow providing access token for login
+ Only accept invitations from arnie admins
- AwightPlugin
+ DatePlugin
- VersionsPlugin
- Respond like !arnie to a mention
- Provide a `!arnie !help` command
- !pings command to print a list of every time you were pinged (in this room?)
  in the last few days/weeks
- Don't crash if e.g. we get kicked out of a room, or other Matrix errors.
- Allow escaping quotes in quoted arguments
- Figure out why Arnie was double-greeting us in the #andybalaam
- Parse messages once and pass in to plugins
- Enhance greet plugin
  - Detect firstness and say e.g. "Awight, first", "Awight seconds"
  - Variations on first greeting e.g. "Awight, first-aaaaa"
- Can we test the tokio::select! call in bot.rs?
- Think about time zones for !greet
- Create a Redis crypto store
  + Implementation
  - Debug store_passphrase in Redis crypto store - it didn't work when I tried
  - Address PR comments
  - Move into own repo
- !pingme command
- Permissions to add and remove arnie quotes
- Prevent races by implementing pipe() on the DatabaseWrapper
- Other arnie features like handling styling or newlines in !arnie
- Allow verifying users
- Ensure admins are verified
