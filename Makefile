all: test

fmt:
	cargo fmt

compile:
	cargo build

test: fmt compile
	cargo test

build-release: test
	cargo build --release

run:
	cargo run

setup-rust:
	curl https://sh.rustup.rs -sSf | sh
	PATH=${HOME}/.cargo/bin:${PATH} rustup component add rustfmt

doc:
	rustup doc
	cargo doc --open
