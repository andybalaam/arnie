# Arnie

Arnie is a Matrix bot.

## Development

To set up:

* [Install rust](https://www.rust-lang.org/tools/install)
* Install prerequisites:
```sh
rustup component add rustfmt
```

Then, to compile and run tests:

```sh
cargo test
```

To compile a release version:

```sh
cargo build --release
```

To run:

```sh
$ ... supply lots of environment variables - see later ... \
  cargo run
```

Below, we assume the compiled arnie is in your path, but you can use `cargo run`
instead of `arnie` in your dev environment.

## Running

### Before you start: register a user and get a recovery key

Before you start Arnie, you should manually register a Matrix user, set up a
recovery key, and write it down.

### First time: supply credentials

The first time you run Arnie you must log in by providing the login
details for your existing Matrix user:

```sh
$ ARNIE_REDIS_URL="redis://localhost:6379" \
  ARNIE_MATRIX_HOMESERVER_URL="http://localhost:8008" \
  ARNIE_MATRIX_USERNAME="myusername" \
  ARNIE_MATRIX_PASSWORD="mypassword" \
  ARNIE_MATRIX_RECOVERY_KEY="abCD 1q3q ..." \
  ARNIE_MATRIX_ADMIN_USERS="@me:example.com,@them:example.com" \
  arnie
>>> !arnie
Get out.
```

**You should only do this once**, to avoid exposing the credentials and recovery
key more than necessary. After that, Arnie will store his cryptographic identity
in the Redis store.

After you have logged in, you should find out the access token by sending Arnie
a message on the command line, or as one of the admin users, like this:

```
>>> !access_token
syt_YXJuaWUtd...
```

You should store this access token since it allows you to log in later.

### After that: supply access token

Arnie stores your username, homeserver, device ID etc. in the database, so to
log in later, you just need the access token you received in the previous section.

```sh
$ ARNIE_REDIS_URL="redis://localhost:6379" \
  ARNIE_MATRIX_ACCESS_TOKEN="syt_YXJuaW..." \
  ARNIE_MATRIX_ADMIN_USERS="@me:example.com,@them:example.com" \
  arnie
>>> !arnie
Get to the choppa!
```

## Redis URL

You should normally provide `ARNIE_REDIS_URL` as in the examples above. If you
miss it out, Arnie uses an in-memory database and will forget everything about
you when he shuts down.

## DB prefix

If you want to share your Redis DB with other projects, or other instances of
Arnie, you should provide the `ARNIE_DB_PREFIX`:

```sh
$ ARNIE_DB_PREFIX="arniebot|" \
  ... other variables ... \
  arnie
```

You will feel much better about your life if you include `|` at the end of the
prefix you use.

## Store passphrase

If you want the information within your Matrix crypto store to be encrypted by a
store passphrase, supply the `ARNIE_MATRIX_STORE_PASSPHRASE`:

```sh
$ ARNIE_MATRIX_STORE_PASSPHRASE="my_secret_password" \
  ... other variables ... \
  arnie
```

## Admin users

Admins have total power over Arnie. You probably should only have one or two of them.

Arnie always considers the user who can type into his command line console to be
an admin.

To name specific Matrix users who are considered admins, provide a
comma-separated list of their Matrix IDs in
`ARNIE_MATRIX_ADMIN_USERS`:

```sh
$ ARNIE_MATRIX_ADMIN_USERS="@u1:example.com,@u2:example.com" \
  ... other variables ... \
  arnie
```

## Not connecting to Matrix

If you don't want Matrix support at all, you can supply `ARNIE_ENABLE_MATRIX`:

```sh
$ ARNIE_ENABLE_MATRIX=false \
  ... other variables ... \
  arnie
```

but right now Matrix is the only messaging platform that is supported, so you
will have to enjoy talking to Arnie on the command line.

## Dockerizing Arnie

We built him inside a Docker container: rust:buster using:

```sh
cargo build --release
```

Then built his own container using his binary with the dockerfile "Dockerfile-arm64-rust-buster"

You don't need to do it this way, just as long as you know here his binary is.

This dockerfile has  an entrypoint that connects to a redis server in K8S that is in a different namespace:

```sh
arnie -d "arniebot|" -r redis://redis.redis.svc.cluster.local
```

You can of course run everything in the same namespace if you so wish.

Push that container to a repo, then we are ready to use a Helm chart to deploy him.

## Deploying Arnie using Helm

A sample Helm chart is included.

In templates/arnie.yaml RUST_BACKTRACE is set to full for logging purposes, this can be removed if not wanted.

In templates/redis-service.yaml is an example of how to connect to a redis in a different namespace.

In Chart.yaml, the appVersion should be changed to reflect the version of Arnie that has been built.

In values.yaml, the image value should be changed to reflect your repo and image name.

## Releases

To make a release:

* Update `Cargo.toml` changing the version number
* Run `cargo test` to generate `Cargo.lock`
* Update CHANGELOG.md
* Commit the changes and push
* Create the tag e.g. `git tag 0.3.1 && git push --tags`

## License

Copyright 2019-2024 Andy Balaam and the Arnie contributors.

Released under the AGPLv3 license. See [LICENSE](LICENSE) for info.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
