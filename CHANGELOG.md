# Changelog for Arnie

## [1.6.0] - 2024-12-12

* Date plugin

## [1.5.0] - 2024-11-14

* Only join rooms if invited by an admin

## [1.4.1] - 2024-11-04

* Depend on a non-local matrix-sdk-redis

## [1.4.0] - 2024-10-31

* Massive login improvements: supply recovery key first time,
  then just access token from then on.

## [1.3.0] - 2024-07-15

* Switch to rustls

## [1.2.0] - 2024-07-04

* Support saying things with different case e.g. !Arnie uses title case

## [1.1.0] - 2024-06-13

* Provide a !todo command for remembering tasks

## [1.0.0] - 2024-04-18

* Allow multiple personalities to exist

## [0.14.0] - 2024-03-15

* Restrict greetings to the room you are in

## [0.13.0] - 2024-03-04

* Allow setting the time of a greeting

## [0.12.0] - 2024-01-31

* Allow multiple greetings

## [0.11.0] - 2023-12-18

* Fix the bug where Arnie always greets at the same time
* Allow customising the greeting Arnie makes

## [0.10.0] - 2023-11-13

* Randomise when Arnie greets us over a 1hr period

## [0.9.0] - 2023-11-13

* Add a greeter plugin to say Awight every day

## [0.8.0] - 2023-10-10

### Added

* Allow arnie to SHOUT if you shout at him

## [0.7.0] - 2023-09-18

### Added

* Auto-join rooms we are invited to

### Fixed

* Removed ability to goldstar yourself

## [0.6.0] - 2023-07-03

### Added

* Basic implementation of a `!goldstar` command

## [0.5.3] - 2023-06-27

### Fixed

* Fix compile warnings

## [0.5.2] - 2023-06-27

### Added

* Experiments with a goldstar plugin (not ready yet)
* Log an error if we try to send to a room we are not a member of

## [0.5.1] - 2023-06-05

### Fixed

* Don't crash if we fail to send a receipt

## [0.5.0] - 2023-05-22

### Added

* `!arnie !list` to show all of Arnie's quotes
* `!arnie !remove` to remove a quote
* `!arnie !add` to add a quote
* `!arnie !version` to give version info
